﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlDevice
	{
		public int DeviceId { get; }
		public string DeviceDesignator { get; }
		public string DeviceSoftwareVersion { get; }
		public string ClientName { get; }
		public string DeviceSerialNumber { get; }
		public string DeviceStructureLabel { get; }
		public string DeviceLocalizationLabel { get; }
		public IsoXmlDeviceValuePresentation[] DeviceValuePresentations { get; }
		public IsoXmlDeviceProcessData[] DeviceProcessData { get; }
		public IsoXmlDeviceElement[] DeviceElements { get; }

		public IsoXmlDevice(int deviceId, string deviceDesignator, string deviceSoftwareVersion,
			string clientName, string deviceSerialNumber, string deviceStructureLabel,
			string deviceLocalizationLabel, IsoXmlDeviceValuePresentation[] deviceValuePresentations,
			IsoXmlDeviceProcessData[] deviceProcessData,
			IsoXmlDeviceElement[] deviceElements)
		{
			DeviceId = deviceId;
			DeviceDesignator = deviceDesignator;
			DeviceSoftwareVersion = deviceSoftwareVersion;
			ClientName = clientName;
			DeviceSerialNumber = deviceSerialNumber;
			DeviceStructureLabel = deviceStructureLabel;
			DeviceLocalizationLabel = deviceLocalizationLabel;
			DeviceValuePresentations = deviceValuePresentations;
			DeviceProcessData = deviceProcessData;
			DeviceElements = deviceElements;
		}
	}
}