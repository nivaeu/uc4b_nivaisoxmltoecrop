﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlTask
	{
		public int TaskId { get; }
		//public string TaskDesignator { get;}
		//public int? CustomerIdRef { get;}
		public int? FarmIdRef { get; }
		public int? PartFieldIdRef { get; }
		//public int? ResponsibleWorkerIdRef { get; }
		public int TaskStatus { get; set; } //todo: enum
		public byte? DefaultTreatmentZoneCode { get; }
		public byte? PositionLostTreatmentZoneCode { get; }
		public byte? OutOfFieldTreatmentZoneCode { get; }

		//public IsoXmlDataLogTrigger[] DataLogTriggers { get; set; }

		public IsoXmlTime[] Times { get; }

		//public IsoXmlTimeLog[] TimeLogs { get; set; }

		//public IsoXmlDeviceAllocation[] DeviceAllocations { get; set; }

		//public IsoXmlWorkerAllocation[] WorkerAllocations { get; set; }

		//public IsoXmlConnection[] Connections { get; set; }

		//public IsoXmlProductAllocation[] ProductAllocations { get; set; }

		//public IsoXmlCommentAllocation[] CommentAllocations { get; set; }

		//public IsoXmlControlAssignment[] ControlAssignments { get; set; }

		//public IsoXmlGuidanceAllocation[] GuidanceAllocations { get; set; }

		//public IsoXmlGrid Grid { get; }

		//public IsoXmlTreatmentZone[] TreatmentZone { get; }

		public IsoXmlTask(
			int taskId, int taskStatus, int? farmIdRef, int? partFieldIdRef, 
			byte? defaultTreatmentZoneCode, byte? positionLostTreatmentZoneCode,
			byte? outOfFieldTreatmentZoneCode, IsoXmlTime[] times)
		{
			TaskId = taskId;
			FarmIdRef = farmIdRef;
			PartFieldIdRef = partFieldIdRef;
			TaskStatus = taskStatus;
			DefaultTreatmentZoneCode = defaultTreatmentZoneCode;
			PositionLostTreatmentZoneCode = positionLostTreatmentZoneCode;
			OutOfFieldTreatmentZoneCode = outOfFieldTreatmentZoneCode;
			Times = times;
		}
	}
}