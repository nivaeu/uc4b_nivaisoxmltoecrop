﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlPosition
	{
		public IsoXmlPosition(decimal north, decimal east, int status, long? up)
		{
			PositionNorth = north;
			PositionEast = east;
			PositionStatus = status;
			PositionUp = up;
		}

		public decimal PositionNorth { get; }
		public decimal PositionEast { get; }
		public long? PositionUp { get; }
		public int PositionStatus { get; } //todo: enum
		public decimal PDOP { get; }
		public decimal HDOP { get; }
		public byte NumberOfSatellites { get; }
	}
}