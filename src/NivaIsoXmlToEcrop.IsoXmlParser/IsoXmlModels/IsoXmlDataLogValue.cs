﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlDataLogValue
	{
		public IsoXmlDataLogValue(
			int processDataDdi, long processDataValue, int deviceElementIdRef)
		{
			ProcessDataDdi = processDataDdi;
			ProcessDataValue = processDataValue;
			DeviceElementIdRef = deviceElementIdRef;
		}

		public int ProcessDataDdi { get; }
		public long ProcessDataValue { get; }
		public int DeviceElementIdRef { get; }
		public ulong? DataLogPgn { get; }
		public byte? DataLogPgnStartBit { get; }
		public byte? DataLogPgnStopBit { get; }
	}
}