﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlProduct
	{
		public int Id { get; }
		public string ProductDesignator { get; }
		public int? DdiValue { get; }
		public int? ProductGroupIdRef { get; }

		public IsoXmlProduct(int id, string productDesignator, int? ddiValue, int? productGroupIdRef)
		{
			Id = id;
			ProductDesignator = productDesignator;
			DdiValue = ddiValue;
			ProductGroupIdRef = productGroupIdRef;
		}
	}
}