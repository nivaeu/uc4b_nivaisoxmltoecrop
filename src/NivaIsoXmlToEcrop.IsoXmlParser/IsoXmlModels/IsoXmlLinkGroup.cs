﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlLinkGroup
	{
		public IsoXmlLinkGroup(int id, byte type, string manufacturerGln, string linkGroupNamespace,
			IsoXmlLink[] links)
		{
			Id = id;
			Type = type;
			ManufacturerGln = manufacturerGln;
			LinkGroupNamespace = linkGroupNamespace;
			Links = links;
		}

		public int Id { get; }
		public byte Type { get; }
		public string ManufacturerGln { get; }
		public string LinkGroupNamespace { get; }
		public IsoXmlLink[] Links { get; }
	}
}