﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlTaskData
	{
		public int VersionMajor { get; } //todo: evt enum
		public int VersionMinor { get; }
		public string ManagementSoftwareManufacturer { get; }
		public string ManagementSoftwareVersion { get; }
		public string TaskControllerManufacturer { get;  }
		public string TaskControllerVersion { get; }
		public string DataTransferOrigin { get; } //todo: evt enum
		public IsoXmlTask[] Tasks { get; }
		public IsoXmlPartField[] PartFields { get; }
		public IsoXmlValuePresentation[] ValuePresentations { get; }
		public IsoXmlProductGroup[] ProductGroups { get; }
		public IsoXmlProduct[] Products { get; }
		public IsoXmlDevice[] Devices { get; }
		public IsoXmlAttachedFile[] ExtraFiles { get; }

		public IsoXmlTaskData(int versionMajor,
			int versionMinor,
			string managementSoftwareManufacturer,
			string managementSoftwareVersion,
			string taskControllerManufacturer,
			string taskControllerVersion,
			string dataTransferOrigin,
			IsoXmlTask[] tasks,
			IsoXmlPartField[] partFields,
			IsoXmlValuePresentation[] valuePresentations,
			IsoXmlProductGroup[] productGroups,
			IsoXmlProduct[] products,
			IsoXmlDevice[] devices,
			IsoXmlAttachedFile[] extraFiles)
		{
			VersionMajor = versionMajor;
			VersionMinor = versionMinor;
			ManagementSoftwareManufacturer = managementSoftwareManufacturer;
			ManagementSoftwareVersion = managementSoftwareVersion;
			TaskControllerManufacturer = taskControllerManufacturer;
			TaskControllerVersion = taskControllerVersion;
			DataTransferOrigin = dataTransferOrigin;
			Tasks = tasks;
			PartFields = partFields;
			ValuePresentations = valuePresentations;
			ProductGroups = productGroups;
			Products = products;
			Devices = devices;
			ExtraFiles = extraFiles;
		}
	}
}