﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlDeviceElement
	{
		public int DeviceElementId { get; }
		public ushort DeviceElementObjectId { get; }
		public int DeviceElementType { get; set; } //todo: enum ?
		public string DeviceElementDesignator { get; }
		public ushort DeviceElementNumber { get; }
		public ushort ParentObjectId { get; }

		public IsoXmlDeviceElement(
			int deviceElementId,
			ushort deviceElementObjectId,
			int deviceElementType,
			string deviceElementDesignator,
			ushort deviceElementNumber,
			ushort parentObjectId)
		{
			DeviceElementId = deviceElementId;
			DeviceElementObjectId = deviceElementObjectId;
			DeviceElementType = deviceElementType;
			DeviceElementDesignator = deviceElementDesignator;
			DeviceElementNumber = deviceElementNumber;
			ParentObjectId = parentObjectId;
		}
	}
}