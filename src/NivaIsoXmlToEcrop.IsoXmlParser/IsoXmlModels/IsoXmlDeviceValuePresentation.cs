﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlDeviceValuePresentation
	{
		public ushort DeviceValuePresentationObjectId { get; }
		public long Offset { get; }
		public decimal Scale { get; }
		public byte NumberOfDecimals { get; }
		public string UnitDesignator { get; }

		public IsoXmlDeviceValuePresentation(
			ushort deviceValuePresentationObjectId,
			long offset,
			decimal scale,
			byte numberOfDecimals,
			string unitDesignator)
		{
			DeviceValuePresentationObjectId = deviceValuePresentationObjectId;
			Offset = offset;
			Scale = scale;
			NumberOfDecimals = numberOfDecimals;
			UnitDesignator = unitDesignator;
		}
	}
}