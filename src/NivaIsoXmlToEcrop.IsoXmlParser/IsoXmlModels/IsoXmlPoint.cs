﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlPoint
	{
		public int PointType { get; } //todo: make enum
		public string PointDesignator { get; }
		public decimal PointNorth { get; }
		public decimal PointEast { get; }

		public IsoXmlPoint(
			int pointType, string pointDesignator, decimal pointNorth, decimal pointEast)
		{
			PointType = pointType;
			PointDesignator = pointDesignator;
			PointNorth = pointNorth;
			PointEast = pointEast;
		}
	}
}