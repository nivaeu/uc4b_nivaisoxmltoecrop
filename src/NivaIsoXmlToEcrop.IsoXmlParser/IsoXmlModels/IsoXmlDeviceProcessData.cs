﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlDeviceProcessData
	{
		public ushort DeviceProcessDataObjectId { get; }
		public int DeviceProcessDataDDI { get; }
		public byte DeviceProcessDataProperty { get; }
		public int DeviceProcessDataTriggerMethods { get; }
		public string DeviceProcessDataDesignator { get; }
		public ushort? DeviceValuePresentationObjectId { get; }

		public IsoXmlDeviceProcessData(ushort deviceProcessDataObjectId, int deviceProcessDataDdi, byte deviceProcessDataProperty, int deviceProcessDataTriggerMethods, string deviceProcessDataDesignator, ushort? deviceValuePresentationObjectId)
		{
			DeviceProcessDataObjectId = deviceProcessDataObjectId;
			DeviceProcessDataDDI = deviceProcessDataDdi;
			DeviceProcessDataProperty = deviceProcessDataProperty;
			DeviceProcessDataTriggerMethods = deviceProcessDataTriggerMethods;
			DeviceProcessDataDesignator = deviceProcessDataDesignator;
			DeviceValuePresentationObjectId = deviceValuePresentationObjectId;
		}
	}
}