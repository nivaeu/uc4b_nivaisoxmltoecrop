﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlPartField
	{
		public int PartFieldId { get; }
		public string PartFieldCode { get; }
		public string PartFieldDesignator { get; }
		public ulong PartFieldArea { get; }
		public int? CustomerIdRef { get; }
		public int? FarmIdRef { get; }

		public IsoXmlPolygon[] Polygons { get; }

		public IsoXmlPartField(
			int partFieldId, string partFieldCode, string partFieldDesignator,
			ulong partFieldArea, int? customerIdRef, int? farmIdRef, IsoXmlPolygon[] polygons)
		{
			PartFieldId = partFieldId;
			PartFieldCode = partFieldCode;
			PartFieldDesignator = partFieldDesignator;
			PartFieldArea = partFieldArea;
			CustomerIdRef = customerIdRef;
			FarmIdRef = farmIdRef;
			Polygons = polygons;
		}
	}
}