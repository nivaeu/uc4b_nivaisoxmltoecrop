﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels
{
	public class IsoXmlGrid
	{
		public IsoXmlGrid(
			decimal northPosition, decimal eastPosition,
			double gridCellNorthSize, double gridCellEastSize,
			ulong gridMaximumColumn, ulong gridMaximumRow,
			string filename, byte gridType, byte treatmentZoneCode)
		{
			GridMinimumNorthPosition = northPosition;
			GridMinimumEastPosition = eastPosition;
			GridCellNorthSize = gridCellNorthSize;
			GridCellEastSize = gridCellEastSize;
			GridMaximumColumn = gridMaximumColumn;
			GridMaximumRow = gridMaximumRow;
			Filename = filename;
			GridType = gridType;
			TreatmentZoneCode = treatmentZoneCode;
		}

		public decimal GridMinimumNorthPosition { get; }
		public decimal GridMinimumEastPosition { get; }
		public double GridCellNorthSize { get; }
		public double GridCellEastSize { get; }
		public ulong GridMaximumColumn { get; }
		public ulong GridMaximumRow { get; }
		public string Filename { get; } //todo: Grid file name
		public byte GridType { get; }
		public byte TreatmentZoneCode { get; }
	}
}