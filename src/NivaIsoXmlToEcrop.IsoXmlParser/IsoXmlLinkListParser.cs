﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using Newtonsoft.Json;
using NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal class IsoXmlLinkListParser : JsonIsoXmlIdParser
	{
		private const string LinkList = "LINKLIST.XML";

		public IsoXmlLinkList Parse(IsoXmlFilesInput isoXmlFiles)
		{
			if (isoXmlFiles == null || !isoXmlFiles.ExtraFiles.ContainsKey(LinkList))
			{
				return null;
			}

			var linkList = isoXmlFiles.ExtraFiles[LinkList];
			if (linkList == null)
			{
				return null;
			}

			try
			{
				var json = XmlToJsonStringParser.Parse(linkList);
				var data = JsonConvert.DeserializeObject<JsonIsoXml11783LinkList>(json);
				return ParseLinkList(data);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return null;
			}
		}

		internal IsoXmlLinkList ParseLinkList(JsonIsoXml11783LinkList data)
		{
			var linkGroup = data.JsonIsoXmlLinkList.LinkGroups.Select(ParseLinkGroup).ToArray();
			return new IsoXmlLinkList(
				data.JsonIsoXmlLinkList.ManagementSoftwareManufacturer,
				linkGroup
				);
		}

		internal IsoXmlLinkGroup ParseLinkGroup(JsonIsoXmlLinkGroup input)
		{
			if (string.IsNullOrWhiteSpace(input.LinkGroupId))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.LinkGroupType))
			{
				return null;
			}

			var id = GetId(input.LinkGroupId, "LGP");
			var linkGroupType = byte.Parse(input.LinkGroupType);
			var links = input.Links.Select(ParseLink).ToArray();
			return new IsoXmlLinkGroup(
				id, linkGroupType, input.ManufacturerGln, input.LinkGroupNamespace, links);
		}

		internal IsoXmlLink ParseLink(JsonIsoXmlLink input)
		{
			if (string.IsNullOrWhiteSpace(input.ObjectIdRef))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.LinkValue))
			{
				return null;
			}

			var (id, type) = GetIdAndType(input.ObjectIdRef);

			return new IsoXmlLink(id, type, input.LinkValue);
		}
	}
}