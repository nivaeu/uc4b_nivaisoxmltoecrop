﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal class IsoXmlTaskParser
	{
		private const decimal CoordinateConverterFactor = 10000000;
		public JsonIsoXmlTaskData ParseJsonIsoXml(IsoXmlFilesInput isoXmlFiles)
		{
			var parser = new IsoXmlTaskParser();
			var taskData = parser.ParseTask(isoXmlFiles.TaskDataXml);
			foreach (var jsonIsoXmlTask in taskData.TaskData.Tasks)
			{
				if (jsonIsoXmlTask.Grid != null)
				{
					var gridFile = isoXmlFiles.GridFiles[jsonIsoXmlTask.Grid.Filename];
					var grid = parser.ParseIsoXmlGridData(jsonIsoXmlTask, gridFile);
					//todo: use grid
				}

				if (jsonIsoXmlTask.TimeLog != null)
				{
					var times = new List<JsonIsoXmlTime>();
					times.AddRange(jsonIsoXmlTask.Time);

					foreach (var jsonIsoXmlTimeLog in jsonIsoXmlTask.TimeLog)
					{
						var bytes = isoXmlFiles.TimeLogFiles[$"{jsonIsoXmlTimeLog.Filename}"];
						var time = parser.ParseIsoXmlTimeLogData(bytes.Xml, bytes.Bin);
						times.AddRange(time);
					}

					jsonIsoXmlTask.Time = times.ToArray();
				}
			}

			return taskData; //todo: also return grid
		}

		public JsonIsoXmlTaskData ParseTask(byte[] content)
		{
			try
			{
				var json = XmlToJsonStringParser.Parse(content);
				var taskData = JsonConvert.DeserializeObject<JsonIsoXmlTaskData>(json);
				return taskData;
			}
			catch (Exception e)
			{
				//todo: implement better error handling
				Console.WriteLine(e);
				return null;
			}
		}

		public List<JsonIsoXmlTime> ParseIsoXmlTimeLogData(byte[] xml, byte[] binaryBytes)
		{
			var json = XmlToJsonStringParser.Parse(xml);
			var timeLog = JsonConvert.DeserializeObject<JsonIsoXmlTimeLogData>(json);

			var positionCount = new IsoXmlTimePositionByteCount(timeLog.Time);
			var result = new List<JsonIsoXmlTime>();
			var recordOffset = 0;
			while (recordOffset < binaryBytes.Length)
			{
				var dvlCount = binaryBytes[recordOffset + positionCount.DvlCount];

				var time = CreateIsoXmlTime(binaryBytes, timeLog, positionCount, recordOffset, dvlCount);

				recordOffset = recordOffset + positionCount.Count + 5 * dvlCount;

				result.Add(time);
			}

			return result;
		}

		public GridTreatmentZone[] ParseIsoXmlGridData(JsonIsoXmlTask task, byte[] binaryBytes)
		{
			if (task.Grid.GridType != "1" && task.Grid.GridType != "2")
			{
				throw new NotImplementedException("IsoXml only support GridType 1 and 2");
			}

			if (task.Grid.GridType == "2")
			{
				if (task.TreatmentZone.First().ProcessDataVariable.Length > 1)
				{
					throw new NotImplementedException("");
				}

				return ParseGridType2(binaryBytes, byte.Parse(task.Grid.TreatmentZoneCode));
			}

			var treatmentZoneLookup = new Dictionary<byte, uint>();
			foreach (var zone in task.TreatmentZone)
			{
				var zoneTreatmentZoneCode = byte.Parse(zone.TreatmentZoneCode);
				var processDataValue = uint.Parse(zone.ProcessDataVariable.First().ProcessDataValue);
				treatmentZoneLookup.Add(zoneTreatmentZoneCode, processDataValue);
			}

			var gridCount = binaryBytes.Length;
			var result = new GridTreatmentZone[gridCount];
			for (var i = 0; i < binaryBytes.Length; i += 1)
			{
				var cellId = i;
				var treatmentZone = binaryBytes[i];
				result[cellId] = new GridTreatmentZone(cellId, treatmentZone, treatmentZoneLookup[treatmentZone]);
			}

			return result;
		}

		private GridTreatmentZone[] ParseGridType2(byte[] binaryBytes, byte treatmentZone)
		{
			//todo: only implement to handle task with one product

			const int byteSize = 4;
			var result = new GridTreatmentZone[binaryBytes.Length / byteSize];
			for (int i = 0; i < binaryBytes.Length; i += byteSize)
			{
				var value = BitConverter.ToUInt32(binaryBytes, i);
				result[i / byteSize] = new GridTreatmentZone(i / byteSize, treatmentZone, value);
			}

			return result;
		}

		//todo: when have decided on DTO for parsed IsoXml, should return parsed object
		private JsonIsoXmlTime CreateIsoXmlTime(
			byte[] binaryBytes,
			JsonIsoXmlTimeLogData timeLog,
			IsoXmlTimePositionByteCount isoXmlTimePositionCount,
			int recordOffset,
			int dvlCount)
		{
			var time = new JsonIsoXmlTime
			{
				Position = new JsonIsoXmlPosition[1],
				DataLogValue = new JsonIsoXmlDataLogValue[dvlCount],
				Type = timeLog.Time.Type
			};
			time.Position[0] = new JsonIsoXmlPosition();

			var millisSinceMidnight = BitConverter.ToUInt32(binaryBytes, recordOffset);
			var daysSince1980 = BitConverter.ToUInt16(binaryBytes, recordOffset + 4);
			var date = new DateTime(1980,1,1).AddDays(daysSince1980).AddMilliseconds(millisSinceMidnight);
			time.Start = date.ToString(); //todo

			var position = timeLog.Time.Position[0];
			if (position.PositionNorth == string.Empty)
			{
				var positionNorth = BitConverter.ToUInt32(binaryBytes, recordOffset + isoXmlTimePositionCount.PositionNorth);
				var north = positionNorth/CoordinateConverterFactor;
				time.Position[0].PositionNorth = north.ToString("N9", CultureInfo.InvariantCulture);
			}

			if (position.PositionEast == string.Empty)
			{
				var positionEast = BitConverter.ToUInt32(binaryBytes, recordOffset + isoXmlTimePositionCount.PositionEast);
				var east = positionEast/CoordinateConverterFactor;
				time.Position[0].PositionEast = east.ToString("N10", CultureInfo.InvariantCulture);
			}

			if (position.PositionUp == string.Empty)
			{
				var positionUp = BitConverter.ToUInt32(binaryBytes, recordOffset + isoXmlTimePositionCount.PositionUp);
				time.Position[0].PositionUp = positionUp.ToString();
			}

			if (position.PositionStatus == string.Empty)
			{
				var positionStatus = binaryBytes[recordOffset + isoXmlTimePositionCount.PositionStatus];
				time.Position[0].PositionStatus = positionStatus.ToString();
			}

			if (position.PDOP == string.Empty)
			{
				var PDOP = BitConverter.ToUInt16(binaryBytes, recordOffset + isoXmlTimePositionCount.PDOP);
				time.Position[0].PDOP = PDOP.ToString();
			}

			if (position.HDOP == string.Empty)
			{
				var HDOP = BitConverter.ToUInt16(binaryBytes, recordOffset + isoXmlTimePositionCount.HDOP);
				time.Position[0].HDOP = HDOP.ToString();
			}

			if (position.NumberOfSatellites == string.Empty)
			{
				var satellite = BitConverter.ToUInt16(binaryBytes, recordOffset + isoXmlTimePositionCount.NumberOfSatellites);
				time.Position[0].NumberOfSatellites = satellite.ToString();
			}

			if (dvlCount > 0)
			{
				const int bytesSize = 5;
				var dvlByteCount = bytesSize * dvlCount;
				for (int i = 0; i < dvlByteCount; i += bytesSize)
				{
					var bytePosition = recordOffset + i + isoXmlTimePositionCount.Count;
					var dvlIndex = binaryBytes[bytePosition];
					var temp = new JsonIsoXmlDataLogValue
					{
						ProcessDataValue = BitConverter.ToUInt32(binaryBytes, bytePosition + 1).ToString(),
						ProcessDataDDI = timeLog.Time.DataLogValue[dvlIndex].ProcessDataDDI,
						DeviceElementIdRef = timeLog.Time.DataLogValue[dvlIndex].DeviceElementIdRef
					};
					time.DataLogValue[i / bytesSize] = temp;
				}
			}

			return time;
		}
	}
}