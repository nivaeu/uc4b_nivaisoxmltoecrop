﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlPosition
	{
		[JsonProperty("A")]
		public string PositionNorth { get; set; }
		[JsonProperty("B")]
		public string PositionEast { get; set; }
		[JsonProperty("C")]
		public string PositionUp { get; set; }
		[JsonProperty("D")]
		public string PositionStatus { get; set; }
		[JsonProperty("E")]
		public string PDOP { get; set; }
		[JsonProperty("F")]
		public string HDOP { get; set; }
		[JsonProperty("G")]
		public string NumberOfSatellites { get; set; }
		[JsonProperty("H")]
		public string H { get; set; }

		public string I { get; set; }
	}
}