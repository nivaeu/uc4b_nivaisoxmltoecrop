﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlPartField
	{
		[JsonProperty("A")]
		public string PartFieldId { get; set; }
		[JsonProperty("B")]
		public string PartFieldCode { get; set; }
		[JsonProperty("C")]
		public string PartFieldDesignator { get; set; }
		[JsonProperty("D")]
		public string PartFieldArea { get; set; }
		[JsonProperty("E")]
		public string CustomerIdRef { get; set; }
		[JsonProperty("F")]
		public string FarmIdRef { get; set; }

		[JsonProperty("PLN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlPolygon>))]
		public JsonIsoXmlPolygon[] Polygons { get; set; }
	}
}