﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlDevice
	{
		[JsonProperty("A")]
		public string DeviceId { get; set; }
		[JsonProperty("B")]
		public string DeviceDesignator { get; set; }
		[JsonProperty("C")]
		public string DeviceSoftwareVersion { get; set; }
		[JsonProperty("D")]
		public string ClientName { get; set; }
		[JsonProperty("E")]
		public string DeviceSerialNumber { get; set; }
		[JsonProperty("F")]
		public string DeviceStructureLabel { get; set; }
		[JsonProperty("G")]
		public string DeviceLocalizationLabel { get; set; }

		[JsonProperty("DET")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDeviceElement>))]
		public JsonIsoXmlDeviceElement[] DeviceElements { get; set; }

		[JsonProperty("DPD")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDeviceProcessData>))]
		public JsonIsoXmlDeviceProcessData[] DeviceProcessData { get; set; }

		[JsonProperty("DVP")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDeviceValuePresentation>))]
		public JsonIsoXmlDeviceValuePresentation[] DeviceValuePresentations { get; set; }
	}
}