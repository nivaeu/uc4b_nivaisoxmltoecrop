﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIxoXmlTaskData11783
	{
		public string VersionMajor { get; set; }
		public string VersionMinor { get; set; }
		public string ManagementSoftwareManufacturer { get; set; }
		public string ManagementSoftwareVersion { get; set; }
		public string TaskControllerManufacturer { get; set; }
		public string TaskControllerVersion { get; set; }
		public string DataTransferOrigin { get; set; }

		[JsonProperty("TSK")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlTask>))]
		public JsonIsoXmlTask[] Tasks { get; set; }

		[JsonProperty("PFD")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlPartField>))]
		public JsonIsoXmlPartField[] PartFields { get; set; }

		[JsonProperty("PGP")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlProductGroup>))]
		public JsonIsoXmlProductGroup[] ProductGroups { get; set; }

		[JsonProperty("PDT")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlProduct>))]
		public JsonIsoXmlProduct[] Products { get; set; }

		[JsonProperty("VPN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlValuePresentation>))]
		public JsonIsoXmlValuePresentation[] ValuePresentations { get; set; }

		[JsonProperty("DVC")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDevice>))]
		public JsonIsoXmlDevice[] Devices { get; set; }

		[JsonProperty("AFE")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlAttachedFile>))]
		public JsonIsoXmlAttachedFile[] AttachedFiles { get; set; }
	}
}