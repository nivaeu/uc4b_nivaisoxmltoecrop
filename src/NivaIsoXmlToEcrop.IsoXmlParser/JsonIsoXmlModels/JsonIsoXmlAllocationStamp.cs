﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlAllocationStamp
	{
		[JsonProperty("A")]
		public DateTime? Start { get; set; }
		[JsonProperty("B")]
		public DateTime? Stop { get; set; }
		[JsonProperty("C")]
		public string Duration { get; set; }
		[JsonProperty("D")]
		public string Type { get; set; }

		[JsonProperty("PTN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlPosition>))]
		public JsonIsoXmlPosition[] Position { get; set; }
	}
}