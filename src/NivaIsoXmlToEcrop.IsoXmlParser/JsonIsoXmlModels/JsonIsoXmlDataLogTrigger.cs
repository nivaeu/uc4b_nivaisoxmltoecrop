﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlDataLogTrigger
	{
		[JsonProperty("A")]
		public string DataLogDDI { get; set; }
		[JsonProperty("B")]
		public string DataLogMethod { get; set; }
		[JsonProperty("C")]
		public string DataLogDistanceInterval { get; set; }
		[JsonProperty("D")]
		public string DataLogTimeInterval { get; set; }
		[JsonProperty("E")]
		public string E { get; set; }
		[JsonProperty("F")]
		public string F { get; set; }
		[JsonProperty("G")]
		public string G { get; set; }
		[JsonProperty("H")]
		public string H { get; set; }
		[JsonProperty("I")]
		public string I { get; set; }
		[JsonProperty("J")]
		public string J { get; set; }
		[JsonProperty("K")]
		public string K { get; set; }
		[JsonProperty("L")]
		public string L { get; set; }
	}
}