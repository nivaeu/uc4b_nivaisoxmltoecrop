﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlControlAssignment
	{
		[JsonProperty("A")]
		public string A { get; set; }
		[JsonProperty("B")]
		public string B { get; set; }
		[JsonProperty("C")]
		public string C { get; set; }
		[JsonProperty("D")]
		public string D { get; set; }
		[JsonProperty("E")]
		public string E { get; set; }
		[JsonProperty("F")]
		public string F { get; set; }
		[JsonProperty("G")]
		public string G { get; set; }
		[JsonProperty("ASP")]
		public JsonIsoXmlAllocationStamp AllocationStamp { get; set; }
	}
}