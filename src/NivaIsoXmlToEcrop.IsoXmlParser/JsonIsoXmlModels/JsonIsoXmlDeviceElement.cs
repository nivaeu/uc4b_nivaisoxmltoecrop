﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlDeviceElement
	{
		[JsonProperty("A")]
		public string DeviceElementId { get; set; }
		[JsonProperty("B")]
		public string DeviceElementObjectId { get; set; }
		[JsonProperty("C")]
		public string DeviceElementType { get; set; }
		[JsonProperty("D")]
		public string DeviceElementDesignator { get; set; }
		[JsonProperty("E")]
		public string DeviceElementNumber { get; set; }
		[JsonProperty("F")]
		public string ParentObjectId { get; set; }

		[JsonProperty("DOR")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDeviceObjectReference>))]
		public JsonIsoXmlDeviceObjectReference[] DeviceObjectReferences { get; set; }
	}
}