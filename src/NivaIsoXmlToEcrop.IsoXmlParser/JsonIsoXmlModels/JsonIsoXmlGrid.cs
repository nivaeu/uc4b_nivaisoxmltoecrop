﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlGrid
	{
		[JsonProperty("A")]
		public string GridMinimumNorthPosition { get; set; }
		[JsonProperty("B")]
		public string GridMinimumEastPosition { get; set; }
		[JsonProperty("C")]
		public string GridCellNorthSize { get; set; }
		[JsonProperty("D")]
		public string GridCellEastSize { get; set; }
		[JsonProperty("E")]
		public string GridMaximumColumn { get; set; }
		[JsonProperty("F")]
		public string GridMaximumRow { get; set; }
		[JsonProperty("G")]
		public string Filename { get; set; }
		[JsonProperty("H")]
		public string FileLength { get; set; }
		[JsonProperty("I")]
		public string GridType { get; set; }
		[JsonProperty("J")]
		public string TreatmentZoneCode { get; set; }
	}
}