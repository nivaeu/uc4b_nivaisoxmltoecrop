﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlProcessDataVariable
	{
		[JsonProperty("A")]
		public string ProcessDataDDI { get; set; }
		[JsonProperty("B")]
		public string ProcessDataValue { get; set; }
		[JsonProperty("C")]
		public string ProductIdRef { get; set; }
		[JsonProperty("D")]
		public string DeviceElementIdRef { get; set; }
		[JsonProperty("E")]
		public string ValuePresentationIdRef { get; set; }
	}
}