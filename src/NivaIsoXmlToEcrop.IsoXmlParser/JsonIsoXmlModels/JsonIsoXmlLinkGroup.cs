﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlLinkGroup
	{
		[JsonProperty("A")]
		public string LinkGroupId { get; set; }
		[JsonProperty("B")]
		public string LinkGroupType { get; set; }
		[JsonProperty("C")]
		public string ManufacturerGln { get; set; }
		[JsonProperty("D")]
		public string LinkGroupNamespace { get; set; }

		[JsonProperty("LNK")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlLink>))]
		public JsonIsoXmlLink[] Links { get; set; }
	}
}