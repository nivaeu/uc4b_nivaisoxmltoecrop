﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlTask
	{
		[JsonProperty("A")]
		public string TaskId { get; set; }
		[JsonProperty("B")]
		public string TaskDesignator { get; set; }
		[JsonProperty("C")]
		public string CustomerIdRef { get; set; }
		[JsonProperty("D")]
		public string FarmIdRef { get; set; }
		[JsonProperty("E")]
		public string PartFieldIdRef { get; set; }
		[JsonProperty("F")]
		public string ResponsibleWorkerIdRef { get; set; }
		[JsonProperty("G")]
		public string TaskStatus { get; set; }
		[JsonProperty("H")]
		public string DefaultTreatmentZoneCode { get; set; }
		[JsonProperty("I")]
		public string PositionLostTreatmentZoneCode { get; set; }
		[JsonProperty("J")]
		public string OutOfFieldTreatmentZoneCode { get; set; }

		[JsonProperty("DLT")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDataLogTrigger>))]
		public JsonIsoXmlDataLogTrigger[] DataLogTrigger { get; set; }

		[JsonProperty("TIM")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlTime>))]
		public JsonIsoXmlTime[] Time { get; set; }

		[JsonProperty("TLG")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlTimeLog>))]
		public JsonIsoXmlTimeLog[] TimeLog { get; set; }

		[JsonProperty("DAN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlDeviceAllocation>))]
		public JsonIsoXmlDeviceAllocation[] DeviceAllocation { get; set; }

		[JsonProperty("WAN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlWorkerAllocation>))]
		public JsonIsoXmlWorkerAllocation[] WorkerAllocation { get; set; }

		[JsonProperty("CNN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlConnection>))]
		public JsonIsoXmlConnection[] Connection { get; set; }

		[JsonProperty("PAN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlProductAllocation>))]
		public JsonIsoXmlProductAllocation[] ProductAllocation { get; set; }

		[JsonProperty("CAN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlCommentAllocation>))]
		public JsonIsoXmlCommentAllocation[] CommentAllocation { get; set; }

		[JsonProperty("CAT")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlControlAssignment>))]
		public JsonIsoXmlControlAssignment[] ControlAssignment { get; set; }

		[JsonProperty("GAN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlGuidanceAllocation>))]
		public JsonIsoXmlGuidanceAllocation[] GuidanceAllocation { get; set; }

		[JsonProperty("GRD")]
		public JsonIsoXmlGrid Grid { get; set; }

		[JsonProperty("TZN")]
		[JsonConverter(typeof(JsonArrayConverter<JsonIsoXmlTreatmentZone>))]
		public JsonIsoXmlTreatmentZone[] TreatmentZone { get; set; }
	}
}