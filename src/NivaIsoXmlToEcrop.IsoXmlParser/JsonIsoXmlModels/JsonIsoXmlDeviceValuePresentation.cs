﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels
{
	internal class JsonIsoXmlDeviceValuePresentation
	{
		[JsonProperty("A")]
		public string DeviceValuePresentationObjectId { get; set; }
		[JsonProperty("B")]
		public string Offset { get; set; }
		[JsonProperty("C")]
		public string Scale { get; set; }
		[JsonProperty("D")]
		public string NumberOfDecimals { get; set; }
		[JsonProperty("E")]
		public string UnitDesignator { get; set; }
	}
}