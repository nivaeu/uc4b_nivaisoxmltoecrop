﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal class JsonArrayConverter<T> : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(List<T>));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			// This is a hack to ensure that properties serialized TO strings BUT recognized
			// by JSON.NET as DateTimes do not get parsed and subsequent .ToString()ed,
			// losing timezone and tick information
			// See IsoXmlTime class
			reader.DateParseHandling = DateParseHandling.None;
			var token = JToken.Load(reader);
			if (token.Type == JTokenType.Array)
			{
				return token.ToObject<List<T>>().ToArray();
			}
			return new[] { token.ToObject<T>() };
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var list = (List<T>)value;
			if (list.Count == 1)
			{
				value = list[0];
			}
			serializer.Serialize(writer, value);
		}

		public override bool CanWrite => true;
	}
}