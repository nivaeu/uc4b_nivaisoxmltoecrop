﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.IO;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal static class XmlToJsonStringParser
	{
		public static string Parse(byte[] bytes)
		{
			string result;
			using (var ms = new MemoryStream(bytes))
			{
				var xmlDoc = new XmlDocument();
				xmlDoc.Load(ms);

				var builder = new StringBuilder();
				var serializer = JsonSerializer.Create(new JsonSerializerSettings());
				serializer.Serialize(new CustomXmlToJsonWriter(new StringWriter(builder)), xmlDoc);
				result = builder.ToString();
			}

			return result;
		}
	}
}