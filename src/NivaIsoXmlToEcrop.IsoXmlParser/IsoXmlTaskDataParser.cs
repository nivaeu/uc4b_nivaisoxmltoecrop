﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	public class IsoXmlTaskDataParser : IIsoXmlParser
	{
		public IsoXmlTaskData Parse(IsoXmlFilesInput isoXmlFiles)
		{
			var parser = new IsoXmlTaskParser();
			var jsonTaskData = parser.ParseJsonIsoXml(isoXmlFiles);
			var isoXmlParser = new JsonIsoXmlParser();
			var taskData = isoXmlParser.ParseTaskData(jsonTaskData);
			return taskData;
		}

		public IsoXmlLinkList ParseLinkList(IsoXmlFilesInput isoXmlFiles)
		{
			var parser = new IsoXmlLinkListParser();
			var result = parser.Parse(isoXmlFiles);
			return result;
		}
	}

	public interface IIsoXmlParser
	{
		IsoXmlTaskData Parse(IsoXmlFilesInput isoXmlFiles);
		IsoXmlLinkList ParseLinkList(IsoXmlFilesInput isoXmlFiles);
	}
}