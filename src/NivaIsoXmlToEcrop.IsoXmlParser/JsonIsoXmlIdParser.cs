﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal class JsonIsoXmlIdParser
	{
		private const int JsonIdLength = 3;
		public int GetId(string jsonId, string idName)
		{
			var hasSymbol = jsonId.Contains("-");
			var count = idName.Length + (hasSymbol ? 1 : 0);
			var substring = jsonId.Substring(count);

			var id = int.Parse(substring);

			return id;
		}

		public (int Id, string Type) GetIdAndType(string jsonId)
		{
			var hasSymbol = jsonId.Contains("-");
			var count = JsonIdLength + (hasSymbol ? 1 : 0);
			var idSubstring = jsonId.Substring(count);

			var id = int.Parse(idSubstring);
			var type = jsonId.Substring(0, JsonIdLength);
			return (id, type);
		}
	}
}