﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Globalization;
using System.Linq;
using NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal class JsonIsoXmlParser : JsonIsoXmlIdParser
	{
		public IsoXmlValuePresentation ParseValuePresentation(JsonIsoXmlValuePresentation input)
		{
			if (string.IsNullOrWhiteSpace(input.ValuePresentationId))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.Offset))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.Scale))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.NumberOfDecimals))
			{
				return null;
			}

			var id = GetId(input.ValuePresentationId, "VPN");
			var offset = long.Parse(input.Offset);
			var scale = decimal.Parse(input.Scale, CultureInfo.InvariantCulture);
			var numberOdDecimals = byte.Parse(input.NumberOfDecimals);

			return new IsoXmlValuePresentation(id, offset, scale, numberOdDecimals, input.UnitDesignator);
		}

		public IsoXmlProductGroup ParseProductGroup(JsonIsoXmlProductGroup input)
		{
			if (string.IsNullOrWhiteSpace(input.ProductGroupId))
			{
				return null;
			}

			var id = GetId(input.ProductGroupId, "PGP");
			return new IsoXmlProductGroup(id, input.ProductGroupDesignator);
		}

		public IsoXmlProduct ParseProduct(JsonIsoXmlProduct input)
		{
			if (string.IsNullOrWhiteSpace(input.ProductId))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.ProductDesignator))
			{
				return null;
			}

			var id = GetId(input.ProductId, "PDT");
			var ddiValue = string.IsNullOrWhiteSpace(input.QuantityDDI)
				? (int?)null
				: int.Parse(input.QuantityDDI, NumberStyles.HexNumber);
			var productGroupId = string.IsNullOrWhiteSpace(input.ProductGroupIdRef)
				? (int?)null
				: GetId(input.ProductGroupIdRef, "PGP");

			return new IsoXmlProduct(id, input.ProductDesignator, ddiValue, productGroupId);
		}

		public IsoXmlDataLogValue ParseDataLogValue(JsonIsoXmlDataLogValue input)
		{
			if (string.IsNullOrWhiteSpace(input.DeviceElementIdRef))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.ProcessDataDDI))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.ProcessDataValue))
			{
				return null;
			}

			var deviceId = GetId(input.DeviceElementIdRef, "DET");
			var processDataValue = long.Parse(input.ProcessDataValue);
			var processDataDdi = int.Parse(input.ProcessDataDDI, NumberStyles.HexNumber);

			return new IsoXmlDataLogValue(processDataDdi, processDataValue, deviceId);
		}

		public IsoXmlPosition ParsePosition(JsonIsoXmlPosition input)
		{
			if (string.IsNullOrWhiteSpace(input.PositionNorth))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.PositionEast))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.PositionStatus))
			{
				return null;
			}

			var north = decimal.Parse(input.PositionNorth, CultureInfo.InvariantCulture);
			var east = decimal.Parse(input.PositionEast, CultureInfo.InvariantCulture);
			var status = int.Parse(input.PositionStatus);
			var up = string.IsNullOrWhiteSpace(input.PositionUp) ? (long?)null : long.Parse(input.PositionUp);

			return new IsoXmlPosition(north, east, status, up);
		}

		public IsoXmlTreatmentZone ParseTreatmentZone(JsonIsoXmlTreatmentZone input)
		{
			if (string.IsNullOrWhiteSpace(input.TreatmentZoneCode))
			{
				return null;
			}

			var code = byte.Parse(input.TreatmentZoneCode);
			var pdvs = input.ProcessDataVariable.Select(ParseProcessDataVariable).ToArray();
			return new IsoXmlTreatmentZone(code, input.TreatmentZoneDesignator, pdvs);
		}

		public IsoXmlProcessDataVariable ParseProcessDataVariable(JsonIsoXmlProcessDataVariable input)
		{
			if (string.IsNullOrWhiteSpace(input.ProcessDataDDI))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.ProcessDataValue))
			{
				return null;
			}

			var ddi = int.Parse(input.ProcessDataDDI, NumberStyles.HexNumber);
			var ddiValue = long.Parse(input.ProcessDataValue);
			var vpnId = string.IsNullOrWhiteSpace(input.ValuePresentationIdRef)
				? (int?)null
				: GetId(input.ValuePresentationIdRef, "VPN");
			var productId = GetId(input.ProductIdRef, "PDT");
			return new IsoXmlProcessDataVariable(ddi, ddiValue, vpnId, productId);
		}

		public IsoXmlGrid ParseGrid(JsonIsoXmlGrid input)
		{
			if (string.IsNullOrWhiteSpace(input.GridMinimumNorthPosition))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.GridMinimumEastPosition))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.GridCellNorthSize))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.GridCellEastSize))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.GridMaximumColumn))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.GridMaximumRow))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.Filename))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.GridType))
			{
				return null;
			}

			var northPosition = decimal.Parse(input.GridMinimumNorthPosition, CultureInfo.InvariantCulture);
			var eastPosition = decimal.Parse(input.GridMinimumEastPosition, CultureInfo.InvariantCulture);
			var gridCellNorthSize = double.Parse(input.GridCellNorthSize, CultureInfo.InvariantCulture);
			var gridCellEastSize = double.Parse(input.GridCellEastSize, CultureInfo.InvariantCulture);
			var gridMaximumColumn = ulong.Parse(input.GridMaximumColumn);
			var gridMaximumRow = ulong.Parse(input.GridMaximumRow);
			var filename = $"{input.Filename}.BIN";
			var gridType = byte.Parse(input.GridType);
			var treatmentZoneCode = byte.Parse(input.TreatmentZoneCode);

			return new IsoXmlGrid(
				northPosition, eastPosition, gridCellNorthSize, gridCellEastSize,
			gridMaximumColumn, gridMaximumRow, filename, gridType, treatmentZoneCode);
		}

		public IsoXmlPartField ParsePartField(JsonIsoXmlPartField input)
		{
			if (string.IsNullOrWhiteSpace(input.PartFieldId))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.PartFieldDesignator))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.PartFieldArea))
			{
				return null;
			}

			var id = GetId(input.PartFieldId, "PFD");
			var area = ulong.Parse(input.PartFieldArea);
			var customerId = string.IsNullOrWhiteSpace(input.CustomerIdRef)
				? (int?)null
				: GetId(input.CustomerIdRef, "CTR");
			var farmId = string.IsNullOrWhiteSpace(input.FarmIdRef)
				? (int?)null
				: GetId(input.FarmIdRef, "FRM");
			var polygons = input.Polygons?.Select(ParsePolygon).ToArray();
			
			return new IsoXmlPartField(
				id, input.PartFieldCode, input.PartFieldDesignator, area, customerId, farmId, polygons);
		}

		public IsoXmlPolygon ParsePolygon(JsonIsoXmlPolygon input)
		{
			if (string.IsNullOrWhiteSpace(input.PolygonType))
			{
				return null;
			}

			if (input.LineString == null || !input.LineString.Any())
			{
				return null;
			}

			var type = int.Parse(input.PolygonType);
			var lineStrings = input.LineString.Select(ParseLineString).ToArray();
			return new IsoXmlPolygon(type, input.PolygonDesignator, lineStrings);
		}

		public IsoXmlLineString ParseLineString(JsonIsoXmlLineString input)
		{
			if (string.IsNullOrWhiteSpace(input.LineStringType))
			{
				return null;
			}

			var type =  int.Parse(input.LineStringType);
			var point = input.Point.Select(ParsePoint).ToArray();
			return new IsoXmlLineString(type, point);
		}

		public IsoXmlPoint ParsePoint(JsonIsoXmlPoint input)
		{
			if (string.IsNullOrWhiteSpace(input.PointType))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.PointNorth))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.PointEast))
			{
				return null;
			}

			var type = int.Parse(input.PointType);
			var north = decimal.Parse(input.PointNorth, CultureInfo.InvariantCulture);
			var east = decimal.Parse(input.PointEast, CultureInfo.InvariantCulture);
			return new IsoXmlPoint(type, input.PointDesignator, north, east);
		}

		public IsoXmlTime ParseTime(JsonIsoXmlTime input)
		{
			if (string.IsNullOrWhiteSpace(input.Start))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.Type))
			{
				return null;
			}

			var type = (RecordTime)Enum.Parse(typeof(RecordTime), input.Type);
			var isValidEnum = Enum.IsDefined(typeof(RecordTime), type);
			if (!isValidEnum)
			{
				return null;
			}

			var start = DateTime.Parse(input.Start);
			var positions = input.Position?.Select(ParsePosition).ToArray();
			var dataLogValue = input.DataLogValue?.Select(ParseDataLogValue).ToArray();

			if (string.IsNullOrWhiteSpace(input.Stop) && string.IsNullOrWhiteSpace(input.Duration))
			{
				return new IsoXmlTime(start, null, null, type, positions, dataLogValue);
			}

			var stop = string.IsNullOrWhiteSpace(input.Stop)
				? start.AddSeconds(ulong.Parse(input.Duration))
				: DateTime.Parse(input.Stop);
			var duration = string.IsNullOrWhiteSpace(input.Duration)
				? Convert.ToUInt64((stop - start).Seconds)
				: ulong.Parse(input.Duration);
			return new IsoXmlTime(start, stop, duration, type, positions, dataLogValue);
		}

		public IsoXmlDeviceValuePresentation ParseDeviceValuePresentation(
			JsonIsoXmlDeviceValuePresentation input)
		{
			if (string.IsNullOrWhiteSpace(input.DeviceValuePresentationObjectId))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.Scale))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.Offset))
			{
				return null;
			}

			var id = ushort.Parse(input.DeviceValuePresentationObjectId);
			var scale = decimal.Parse(input.Scale, CultureInfo.InvariantCulture);
			var offset = long.Parse(input.Offset);
			var numberOfDecimals = byte.Parse(input.NumberOfDecimals);
			return new IsoXmlDeviceValuePresentation(id, offset, scale, numberOfDecimals, input.UnitDesignator);
		}

		public IsoXmlDeviceProcessData ParseDeviceProcessData(JsonIsoXmlDeviceProcessData input)
		{
			if (string.IsNullOrWhiteSpace(input.DeviceProcessDataObjectId))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.DeviceProcessDataDDI))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.DeviceProcessDataProperty))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.DeviceProcessDataTriggerMethods))
			{
				return null;
			}

			var id = ushort.Parse(input.DeviceProcessDataObjectId);
			var ddiValue = int.Parse(input.DeviceProcessDataDDI, NumberStyles.HexNumber);
			var deviceProcessDataProperty = byte.Parse(input.DeviceProcessDataProperty);
			var deviceProcessDataTriggerMethods = int.Parse(input.DeviceProcessDataTriggerMethods);
			var deviceValuePresentationObjectId = string.IsNullOrWhiteSpace(input.DeviceValuePresentationObjectId)
				? (ushort?)null
				: ushort.Parse(input.DeviceValuePresentationObjectId);
			return new IsoXmlDeviceProcessData(
				id, ddiValue, deviceProcessDataProperty, deviceProcessDataTriggerMethods,
				input.DeviceProcessDataDesignator, deviceValuePresentationObjectId);
		}

		public IsoXmlDeviceElement ParseDeviceElement(JsonIsoXmlDeviceElement input)
		{
			if (string.IsNullOrWhiteSpace(input.DeviceElementId))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.DeviceElementObjectId))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.DeviceElementType))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.DeviceElementNumber))
			{
				return null;
			}

			var id = GetId(input.DeviceElementId, "DET");
			var objectId = ushort.Parse(input.DeviceElementObjectId);
			var type = int.Parse(input.DeviceElementType);
			var elementNumber = ushort.Parse(input.DeviceElementNumber);
			var parentObjectId = ushort.Parse(input.ParentObjectId);

			return new IsoXmlDeviceElement(id, objectId, type, input.DeviceElementDesignator, elementNumber, parentObjectId);
		}

		public IsoXmlDevice ParseDevice(JsonIsoXmlDevice input)
		{
			if (string.IsNullOrWhiteSpace(input.DeviceId))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.ClientName))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.DeviceStructureLabel))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.DeviceLocalizationLabel))
			{
				return null;
			}

			if (input.DeviceElements == null)
			{
				return null;
			}

			var deviceElements = input.DeviceElements.Select(ParseDeviceElement).ToArray();
			var deviceProcessData = input.DeviceProcessData?.Select(ParseDeviceProcessData).ToArray();
			var deviceValuePresentation = input.DeviceValuePresentations?.Select(ParseDeviceValuePresentation).ToArray();

			var id = GetId(input.DeviceId, "DVC");
			return new IsoXmlDevice(
				id, input.DeviceDesignator, input.DeviceSoftwareVersion,
				input.ClientName, input.DeviceSerialNumber, input.DeviceStructureLabel,
				input.DeviceLocalizationLabel, deviceValuePresentation, deviceProcessData,
				deviceElements);
		}

		public IsoXmlAttachedFile ParseAttachedFile(JsonIsoXmlAttachedFile input)
		{
			if (string.IsNullOrWhiteSpace(input.FilenameWithExtension))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.Preserve))
			{
				return null;
			}
			if (string.IsNullOrWhiteSpace(input.FileType))
			{
				return null;
			}

			var preserve = input.Preserve == "2";
			var fileType = byte.Parse(input.FileType);

			return new IsoXmlAttachedFile
			{
				FilenameWithExtension = input.FilenameWithExtension,
				ManufacturerGln = input.ManufacturerGln,
				Preserve = preserve,
				FileType = fileType
			};
		}

		public IsoXmlTask ParseTask(JsonIsoXmlTask input)
		{
			if (string.IsNullOrWhiteSpace(input.TaskId))
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(input.TaskStatus))
			{
				return null;
			}

			var id = GetId(input.TaskId, "TSK");
			var status = int.Parse(input.TaskStatus);
			var farmId = string.IsNullOrWhiteSpace(input.FarmIdRef)
				? (int?)null
				: GetId(input.FarmIdRef, "FRM");
			var fieldId = string.IsNullOrWhiteSpace(input.PartFieldIdRef)
				? (int?) null
				: GetId(input.PartFieldIdRef, "PFD");
			var defaultTreatmentZone = string.IsNullOrWhiteSpace(input.DefaultTreatmentZoneCode)
				? (byte?)null
				: byte.Parse(input.DefaultTreatmentZoneCode);
			var positionLostTreatmentZoneCode = string.IsNullOrWhiteSpace(input.PositionLostTreatmentZoneCode)
				? (byte?)null
				: byte.Parse(input.PositionLostTreatmentZoneCode);
			var outOfFieldTreatmentZoneCode = string.IsNullOrWhiteSpace(input.OutOfFieldTreatmentZoneCode)
				? (byte?)null
				: byte.Parse(input.OutOfFieldTreatmentZoneCode);
			var times = input.Time?.Select(ParseTime).ToArray();
			return new IsoXmlTask(id, status, farmId, fieldId, defaultTreatmentZone, positionLostTreatmentZoneCode, outOfFieldTreatmentZoneCode, times);
		}

		public IsoXmlTaskData ParseTaskData(JsonIsoXmlTaskData input)
		{
			if (string.IsNullOrWhiteSpace(input.TaskData.VersionMajor))
			{
				throw new ArgumentException($"{nameof(input.TaskData.VersionMajor)} missing");
			}
			if (string.IsNullOrWhiteSpace(input.TaskData.VersionMinor))
			{
				throw new ArgumentException($"{nameof(input.TaskData.VersionMinor)} missing");
			}
			if (string.IsNullOrWhiteSpace(input.TaskData.DataTransferOrigin))
			{
				throw new ArgumentException($"{nameof(input.TaskData.DataTransferOrigin)} missing");
			}

			if (input.TaskData.DataTransferOrigin == "2")
			{
				if (string.IsNullOrWhiteSpace(input.TaskData.ManagementSoftwareManufacturer))
				{
					LogInvalidIsoXml(nameof(input.TaskData.ManagementSoftwareManufacturer));
				}
				if (string.IsNullOrWhiteSpace(input.TaskData.ManagementSoftwareVersion))
				{
					LogInvalidIsoXml(nameof(input.TaskData.ManagementSoftwareVersion));
				}
				if (string.IsNullOrWhiteSpace(input.TaskData.TaskControllerManufacturer))
				{
					LogInvalidIsoXml(nameof(input.TaskData.TaskControllerManufacturer));
				}
				if (string.IsNullOrWhiteSpace(input.TaskData.TaskControllerVersion))
				{
					LogInvalidIsoXml(nameof(input.TaskData.TaskControllerVersion));
				}
			}

			var versionMajor = int.Parse(input.TaskData.VersionMajor);
			var versionMinor = int.Parse(input.TaskData.VersionMinor);
			var partFields = input.TaskData.PartFields?.Select(ParsePartField).ToArray();
			var valuePresentations = input.TaskData.ValuePresentations?.Select(ParseValuePresentation).ToArray();
			var productGroups = input.TaskData.ProductGroups?.Select(ParseProductGroup).ToArray();
			var products = input.TaskData.Products?.Select(ParseProduct).ToArray();
			var devices = input.TaskData.Devices.Select(ParseDevice).ToArray();
			var tasks = input.TaskData.Tasks.Select(ParseTask).ToArray();
			var extraFiles = input.TaskData.AttachedFiles?.Select(ParseAttachedFile).ToArray();

			return new IsoXmlTaskData(
				versionMajor,
				versionMinor, input.TaskData.ManagementSoftwareManufacturer,
				input.TaskData.ManagementSoftwareVersion,
				input.TaskData.TaskControllerManufacturer,
				input.TaskData.TaskControllerVersion,
				input.TaskData.DataTransferOrigin,
				tasks,
				partFields,
				valuePresentations,
				productGroups,
				products,
				devices,
				extraFiles);
		}

		private void LogInvalidIsoXml(string param)
		{
			Console.WriteLine($"{param} missing");
		}
	}
}