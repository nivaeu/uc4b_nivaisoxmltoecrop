﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	internal class IsoXmlTimePositionByteCount
	{
		public IsoXmlTimePositionByteCount(JsonIsoXmlTime time)
		{
			var count = 6;
			if (time.Position[0].PositionNorth == string.Empty)
			{
				PositionNorth = count;
				count += 4;
			}
			if (time.Position[0].PositionEast == string.Empty)
			{
				PositionEast = count;
				count += 4;
			}
			if (time.Position[0].PositionUp == string.Empty)
			{
				PositionUp = count;
				count += 4;
			}

			if (time.Position[0].PositionStatus == string.Empty)
			{
				PositionStatus = count;
				count += 1;
			}

			if (time.Position[0].NumberOfSatellites == string.Empty)
			{
				NumberOfSatellites = count;
				count += 1;
			}

			if (time.Position[0].PDOP == string.Empty)
			{
				PDOP = count;
				count += 2;
			}

			if (time.Position[0].HDOP == string.Empty)
			{
				HDOP = count;
				count += 2;
			}

			if (time.Position[0].H == string.Empty)
			{
				count += 4;
			}

			if (time.Position[0].I == string.Empty)
			{
				count += 2;
			}

			DvlCount = count;
			Count = count + 1;
		}

		public int DvlCount { get; }
		public int Count { get; }

		public int HDOP { get; }

		public int PDOP { get; }

		public int NumberOfSatellites { get; }

		public int PositionStatus { get; }

		public int PositionUp { get; }

		public int PositionNorth { get; }

		public int PositionEast { get; }
	}
}