﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace NivaIsoXmlToEcrop.IsoXmlParser
{
	public class IsoXmlFilesInput
	{
		public byte[] TaskDataXml { get; set; }
		public Dictionary<string, byte[]> GridFiles { get; set; }
		public Dictionary<string, (byte[] Xml, byte[] Bin)> TimeLogFiles { get; set; }
		public Dictionary<string, byte[]> ExtraFiles { get; set; }

		public IsoXmlFilesInput()
		{
			GridFiles = new Dictionary<string, byte[]>();
			TimeLogFiles = new Dictionary<string, (byte[] Xml, byte[] Bin)>();
			ExtraFiles = new Dictionary<string, byte[]>();
		}

		private const string TaskDataFileName = "TASKDATA.XML";
		private const string LinkListFileName = "LINKLIST.XML";

		public static IsoXmlFilesInput Create(Stream stream)
		{
			stream.Position = 0;
			var timeLogFiles = new List<(string filename, byte[] content)>();
			IsoXmlFilesInput isoXmlFilesInput = new IsoXmlFilesInput();

			using (var zipArchive = new ZipArchive(stream, ZipArchiveMode.Read))
			{
				foreach (ZipArchiveEntry entry in zipArchive.Entries)
				{
					if (string.Equals(entry.Name, TaskDataFileName, StringComparison.InvariantCultureIgnoreCase))
					{
						isoXmlFilesInput.TaskDataXml = ExtractByte(entry);
						continue;
					}

					if (entry.Name.StartsWith("TLG"))
					{
						timeLogFiles.Add((entry.Name, ExtractByte(entry)));
						continue;
					}

					if (string.Equals(entry.Name, LinkListFileName))
					{
						isoXmlFilesInput.ExtraFiles = new Dictionary<string, byte[]>()
						{
							{LinkListFileName, ExtractByte(entry)}
						};
						continue;
					}

					if (entry.Name.StartsWith("GRD"))
					{
						var key = entry.Name.Substring(0, entry.Name.LastIndexOf(".", StringComparison.InvariantCulture));
						isoXmlFilesInput.GridFiles.Add(key, ExtractByte(entry));
					}
				}
			}

			var tlgs = timeLogFiles
				.Select(file =>
					file.filename.Substring(0, file.filename.LastIndexOf(".", StringComparison.InvariantCulture)))
				.Distinct();
			foreach (var tlg in tlgs)
			{
				var xml = timeLogFiles.First(s => string.Equals(s.filename, $"{tlg}.xml", StringComparison.InvariantCultureIgnoreCase));
				var bin = timeLogFiles.First(s => string.Equals(s.filename, $"{tlg}.bin", StringComparison.InvariantCultureIgnoreCase));
				isoXmlFilesInput.TimeLogFiles.Add(tlg, (xml.content, bin.content));
			}

			return isoXmlFilesInput;
		}

		private static byte[] ExtractByte(ZipArchiveEntry entry)
		{
			using (Stream stream = entry.Open())
			{
				using (var ms = new MemoryStream())
				{
					stream.CopyTo(ms);
					return ms.ToArray();
				}
			}
		}
	}
}