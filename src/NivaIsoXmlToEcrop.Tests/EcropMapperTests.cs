/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using Newtonsoft.Json;
using Niva.Common.Models.ECropReportMessage;
using NivaIsoXmlToEcrop.IsoXmlParser;
using NivaIsoXmlToEcrop.Mapper;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.Tests
{
	public class EcropMapperTests
	{
		[TestCase(1, 755)]
		[TestCase(null, 1005)]
		[TestCase(3, 1145)]
		public void EcropMapper_ValidInput_ThenReturnEcropMessage(int? taskId, int expectedPoints)
		{
			//Arrange
			var ecropJson = File.ReadAllText("TestData/partialEcropMessage.json");
			var partialEcropMessage = JsonConvert.DeserializeObject<EcropMessage>(ecropJson);
			
			var gridFiles = new Dictionary<string, byte[]>
			{
				{"GRD00000", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00000.bin")},
				{"GRD00001", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00001.bin")},
				{"GRD00002", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00002.bin")}
			};
			var timeLogs = new Dictionary<string, (byte[], byte[])>
			{
				{"TLG00001", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.bin"))},
				{"TLG00002", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.bin"))},
				{"TLG00003", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.bin"))},
				{"TLG00004", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.bin"))}
			};
			var isoXmlInput = new IsoXmlFilesInput
			{
				TaskDataXml = File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TASKDATA.XML"),
				GridFiles = gridFiles,
				TimeLogFiles = timeLogs
			};
			var partialEcrop = new PartialEcropInput
			{
				EcropMessage = partialEcropMessage,
				IsoXmlFiles = isoXmlInput,
				IsoXmlTaskId = taskId
			};
			var sut = new EcropMapper();

			//Act
			var result = sut.Map(partialEcrop);

			//Assert
			result.Should().NotBeNull();
			result.ECROPReportMessage.AgriculturalProducerParty.AgriculturalProductionUnit.SpecifiedCropPlot[0]
				.GrownFieldCrop[0].ApplicableCropProductionAgriculturalProcess[0]
				.AppliedSpecifiedAgriculturalApplication[0].AppliedSpecifiedAgriculturalApplicationRate.Length.Should()
				.Be(expectedPoints);
		}

		[Test]
		public void EcropMapper_InvalidInput_ThenReturnNull()
		{
			//Arrange
			var sut = new EcropMapper();

			//Act
			var result = sut.Map(new PartialEcropInput());

			//Assert
			result.Should().BeNull();
		}
	}
}