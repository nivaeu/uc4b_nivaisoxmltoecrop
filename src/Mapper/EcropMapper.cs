﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NetTopologySuite.Geometries;
using Niva.Common.Models.ECropReportMessage;
using NivaIsoXmlToEcrop.IsoXmlParser;
using NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels;

namespace NivaIsoXmlToEcrop.Mapper
{
	public class EcropMapper
	{
		private const int ActualMassPerAreaApplicationRateDdi = 7;
		private const int AccumulatedApplicationInKilogramDdi = 81;
		private const int TotalAreaDdi = 116;

		public EcropMessage Map(PartialEcropInput input)
		{
			if (!input.IsValid())
			{
				return null;
			}

			var parser = new IsoXmlTaskDataParser();
			var taskData = parser.Parse(input.IsoXmlFiles);
			if (taskData == null)
			{
				return null;
			}

			var task = input.IsoXmlTaskId == null
				? taskData.Tasks?[0]
				: taskData.Tasks.FirstOrDefault(t => t.TaskId == input.IsoXmlTaskId);
			if (task == null)
			{
				return null;
			}

			if (task.PartFieldIdRef == null)
			{
				throw new NotImplementedException("Only support isoXml that has partFields");
			}

			var fieldGeometry = FieldGeometry(taskData, task);

			var isoXmlTimes = task.Times
				.Where(time => time.DataLogValue != null
				               && time.DataLogValue.Any(dataLogValue => dataLogValue.ProcessDataDdi == ActualMassPerAreaApplicationRateDdi))
				.ToList();
			if (!isoXmlTimes.Any())
			{
				throw new NotImplementedException($"Only support isoXml with ddi {ActualMassPerAreaApplicationRateDdi}");
			}

			var valuePresentation = IsoXmlDeviceValuePresentation(isoXmlTimes, taskData);

			var rates = new List<AppliedSpecifiedAgriculturalApplicationRate>();
			var pointOutsideField = 0;
			foreach (var isoXmlTime in isoXmlTimes)
			{
				//todo: only use effective time points ?
				if (isoXmlTime.Type != RecordTime.Effective)
				{
					continue;
				}

				if (!isoXmlTime.Position.Any())
				{
					continue;
				}

				var position = isoXmlTime.Position[0];
				var point = new Point(Convert.ToDouble(position.PositionEast), Convert.ToDouble(position.PositionNorth));
				if (!fieldGeometry.Intersects(point))
				{
					pointOutsideField++;
					continue;
				}

				var ddiValue = isoXmlTime.DataLogValue.First(dataLogValue => dataLogValue.ProcessDataDdi == ActualMassPerAreaApplicationRateDdi).ProcessDataValue;

				var rate = new AppliedSpecifiedAgriculturalApplicationRate
				{
					AppliedQuantity = (ddiValue + valuePresentation.Offset) * Convert.ToDouble(valuePresentation.Scale),
					AppliedQuantityUnit = valuePresentation.UnitDesignator,
					AppliedReferencedLocation = new[]
					{
						new ReferencedLocation
						{
							PhysicalSpecifiedGeographicalFeature = new PhysicalSpecifiedGeographicalFeature
							{
								type = PhysicalSpecifiedGeographicalFeatureType.Feature,
								geometry = new PointGeometry()
								{
									coordinates = new [] { point.X, point.Y }
								}
							},
							TypeCode = "TAP",
							ReferenceTypeCode = "CAP"
						}
					}
				};
				rates.Add(rate);
			}

			if (pointOutsideField > 0)
			{
				Console.WriteLine($"Points outside field: {pointOutsideField}");
			}

			var appliedSpecifiedAgriculturalApplication = input.EcropMessage.ECROPReportMessage.AgriculturalProducerParty
				.AgriculturalProductionUnit.SpecifiedCropPlot[0].GrownFieldCrop[0]
				.ApplicableCropProductionAgriculturalProcess[0]
				.AppliedSpecifiedAgriculturalApplication[0];

			appliedSpecifiedAgriculturalApplication.AppliedSpecifiedAgriculturalApplicationRate =
				rates.ToArray();

			var accumulatedApplicationInKilogram = task.Times
				.Where(x => x != null)
				.Select(y => y.DataLogValue?.FirstOrDefault(g => g.ProcessDataDdi == AccumulatedApplicationInKilogramDdi))
				.SingleOrDefault(x => x != null && x.ProcessDataValue > 0);
			appliedSpecifiedAgriculturalApplication.TotalProductAmount = accumulatedApplicationInKilogram?.ProcessDataValue;

			var totalArea = task.Times
				.Where(x => x != null)
				.Select(y => y.DataLogValue?.FirstOrDefault(g => g.ProcessDataDdi == TotalAreaDdi))
				.SingleOrDefault(x => x != null && x.ProcessDataValue > 0);
			appliedSpecifiedAgriculturalApplication.TotalProductAmount = totalArea?.ProcessDataValue;

			var coordsAs3DArray = ConvertGeometryToCoordsAs3DArray(fieldGeometry);
			var fieldFeature = new PhysicalSpecifiedGeographicalFeature
			{
				geometry = new PolygonGeometry
				{
					coordinates = coordsAs3DArray
				}
			};
			input.EcropMessage.ECROPReportMessage.AgriculturalProducerParty.AgriculturalProductionUnit.SpecifiedCropPlot
				.First().SpecifiedReferencedLocation.PhysicalSpecifiedGeographicalFeature = fieldFeature;

			var ecropMessage = input.EcropMessage;
			return ecropMessage;
		}

		private static IsoXmlDeviceValuePresentation IsoXmlDeviceValuePresentation(List<IsoXmlTime> isoXmlTimes, IsoXmlTaskData taskData)
		{
			var deviceId = isoXmlTimes[0].DataLogValue
				.First(f => f.ProcessDataDdi == ActualMassPerAreaApplicationRateDdi)
				.DeviceElementIdRef;
			var device = taskData.Devices.First(g => g.DeviceElements.Any(d => d.DeviceElementId == deviceId));
			var deviceValuePresentationId =
				device.DeviceProcessData.First(data => data.DeviceProcessDataDDI == ActualMassPerAreaApplicationRateDdi);
			var valuePresentation = device.DeviceValuePresentations.First(g =>
				g.DeviceValuePresentationObjectId == deviceValuePresentationId.DeviceValuePresentationObjectId);
			return valuePresentation;
		}

		private static Polygon FieldGeometry(IsoXmlTaskData taskData, IsoXmlTask task)
		{
			var isoXmlPartField = taskData.PartFields.First(field => field.PartFieldId == task.PartFieldIdRef);
			var fieldPolygonPoints = isoXmlPartField.Polygons[0].LineStrings[0].Points
				.Select(
					p => new Coordinate(Convert.ToDouble(p.PointEast), Convert.ToDouble(p.PointNorth)))
				.ToArray();
			var factory = new GeometryFactory();
			var fieldGeometry = factory.CreatePolygon(fieldPolygonPoints);
			return fieldGeometry;
		}

		private static double[][][] ConvertGeometryToCoordsAs3DArray(NetTopologySuite.Geometries.Geometry utmGeometry)
		{
			string geometryLatLongWkt = utmGeometry.AsText();

			// Strip out everything except the coordinates
			var polygonRawText = geometryLatLongWkt.Replace("POLYGON ", "");
			polygonRawText = polygonRawText.Replace("((", "(");
			polygonRawText = polygonRawText.Replace("))", ")");
			polygonRawText = polygonRawText.Replace(", ", ",");
			polygonRawText = polygonRawText.Replace("),(", ")-(");

			string[] rings = polygonRawText.Split('-');
			//3D array consists of [rings][coordinate sets][coordinates]
			//e.g. a polygon with 1 outer ring and 2 inner rings, with each ring having 4 coordinates, has size [3][4][2]
			var coordsAs3DArray = new double[rings.Length][][];

			//Create coordsAs3DArray from linear rings
			for (var ring = 0; ring < rings.Length; ring++)
			{
				string coordRawText = rings[ring].Replace("(", "");
				coordRawText = coordRawText.Replace(")", "");
				// Seperate coordinates to iterate through
				IEnumerable<string> coordStrings = coordRawText.Split(',').Select(coord => coord.Replace(" ", ","));

				// Build list of coordinates
				var coords = new List<double[]>();
				foreach (string coord in coordStrings)
				{
					string[] splt = coord.Split(',');
					double x = Math.Round(double.Parse(splt[0], CultureInfo.InvariantCulture), 6);
					double y = Math.Round(double.Parse(splt[1], CultureInfo.InvariantCulture), 6);

					coords.Add(new[] { x, y });
				}

				coordsAs3DArray[ring] = new double[coords.Count][];

				for (var i = 0; i < coords.Count; i++)
				{
					coordsAs3DArray[ring][i] = new double[2];

					coordsAs3DArray[ring][i][0] = coords.ElementAt(i)[0];
					coordsAs3DArray[ring][i][1] = coords.ElementAt(i)[1];
				}
			}

			return coordsAs3DArray;
		}
	}
}