﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Niva.Common.Models.ECropReportMessage;
using NivaIsoXmlToEcrop.IsoXmlParser;

namespace NivaIsoXmlToEcrop.Mapper
{
	public class PartialEcropInput
	{
		public IsoXmlFilesInput IsoXmlFiles { get; set; }
		public EcropMessage EcropMessage { get; set; }
		public int? IsoXmlTaskId { get; set; }

		public bool IsValid()
		{
			return IsoXmlFiles?.TaskDataXml != null
			       && EcropMessage != null;
		}
	}
}