﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParsePointTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlPoint
			{
				PointType = "2",
				PointNorth = "56.44041992871189",
				PointEast = "9.935954272176934"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParsePoint(input);

			//Assert
			result.Should().NotBeNull();
			result.PointType.Should().Be(2);
			result.PointNorth.Should().Be(56.44041992871189m);
			result.PointEast.Should().Be(9.935954272176934m);
		}
	}
}