﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseGridTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlGrid
			{
				GridMinimumNorthPosition = "56.4394175884933",
				GridMinimumEastPosition = "9.93581843544988",
				GridCellNorthSize = "0.00008785596838518156",
				GridCellEastSize = "0.00016353927243669571",
				GridMaximumColumn = "29",
				GridMaximumRow = "18",
				Filename = "GRD00001",
				FileLength = "2088",
				GridType = "2",
				TreatmentZoneCode = "2"

			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseGrid(input);

			//Assert
			result.Should().NotBeNull();
			result.GridMinimumNorthPosition.Should().Be(56.4394175884933m);
			result.GridMinimumEastPosition.Should().Be(9.93581843544988m);
			result.GridCellNorthSize.Should().Be(0.00008785596838518156);
			result.GridCellEastSize.Should().Be(0.00016353927243669571);
			result.GridMaximumColumn.Should().Be(29);
			result.GridMaximumRow.Should().Be(18);
			result.Filename.Should().Be("GRD00001.BIN");
			result.GridType.Should().Be(2);
			result.TreatmentZoneCode.Should().Be(2);
		}
	}
}