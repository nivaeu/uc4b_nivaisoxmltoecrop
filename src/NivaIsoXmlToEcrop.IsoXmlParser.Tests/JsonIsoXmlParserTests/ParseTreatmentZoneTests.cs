﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseTreatmentZoneTests
	{
		[Test]
		public void ParseTreatmentZone_WhenValidInput_ThenReturnProcessDataVariable()
		{
			//Arrange
			var input = new JsonIsoXmlTreatmentZone
			{
				TreatmentZoneCode = "4",
				TreatmentZoneDesignator = "92 litre/hectare",
				ProcessDataVariable = new JsonIsoXmlProcessDataVariable[1]
				{
					new JsonIsoXmlProcessDataVariable
					{
						ProcessDataDDI = "0001",
						ProcessDataValue = "920000",
						ProductIdRef = "PDT1",
						ValuePresentationIdRef = "VPN8"
					}
				}
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseTreatmentZone(input);

			//Assert
			result.Should().NotBeNull();
			result.TreatmentZoneCode.Should().Be(4);
			result.TreatmentZoneDesignator.Should().Be("92 litre/hectare");
			result.ProcessDataVariables.Length.Should().Be(1);
		}
	}
}