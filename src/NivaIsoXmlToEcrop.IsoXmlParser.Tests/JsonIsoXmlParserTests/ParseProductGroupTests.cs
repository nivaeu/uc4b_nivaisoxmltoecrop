﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseProductGroupTests
	{
		[Test]
		public void ParseProductGroup_WhenValidInput_ThenReturnProductGroup()
		{
			//Arrange
			var input = new JsonIsoXmlProductGroup
			{
				ProductGroupId = "PGP1",
				ProductGroupDesignator = "liquid"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseProductGroup(input);

			//Assert
			result.Should().NotBeNull();
			result.Id.Should().Be(1);
			result.Designator.Should().Be("liquid");
		}
	}
}