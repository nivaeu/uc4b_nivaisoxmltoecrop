﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseDataLogValueTests
	{
		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParseDataLogValue_WhenInvalidDeviceId_ThenReturnNull(string id)
		{
			//Arrange
			var input = new JsonIsoXmlDataLogValue
			{
				DeviceElementIdRef = id,
				ProcessDataValue = "1350000",
				ProcessDataDDI = "0001"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDataLogValue(input);

			//Assert
			result.Should().BeNull();
		}

		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParseDataLogValue_WhenInvalidProcessDataDdi_ThenReturnNull(string processDataDdi)
		{
			//Arrange
			var input = new JsonIsoXmlDataLogValue
			{
				DeviceElementIdRef = "DET1",
				ProcessDataValue = "1350000",
				ProcessDataDDI = processDataDdi
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDataLogValue(input);

			//Assert
			result.Should().BeNull();
		}

		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParseDataLogValue_WhenInvalidProcessDataValue_ThenReturnNull(string processDataValue)
		{
			//Arrange
			var input = new JsonIsoXmlDataLogValue
			{
				DeviceElementIdRef = "DET1",
				ProcessDataValue = processDataValue,
				ProcessDataDDI = "0001"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDataLogValue(input);

			//Assert
			result.Should().BeNull();
		}

		[Test]
		public void ParseDataLogValue_CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlDataLogValue
			{
				DeviceElementIdRef = "DET1",
				ProcessDataValue = "1350000",
				ProcessDataDDI = "0001"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDataLogValue(input);

			//Assert
			result.Should().NotBeNull();
			result.DeviceElementIdRef.Should().Be(1);
			result.ProcessDataValue.Should().Be(1350000);
			result.ProcessDataDdi.Should().Be(1);
			result.DataLogPgn.Should().BeNull();
			result.DataLogPgnStartBit.Should().BeNull();
			result.DataLogPgnStopBit.Should().BeNull();
		}
	}
}