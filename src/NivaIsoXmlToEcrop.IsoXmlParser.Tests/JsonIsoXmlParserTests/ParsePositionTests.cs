﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParsePositionTests
	{
		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParsePosition_WhenInvalidPositionNorth_ThenReturnNull(string north)
		{
			//Arrange
			var input = new JsonIsoXmlPosition
			{
				PositionNorth = north,
				PositionEast = "9.989209",
				PositionStatus = "3"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParsePosition(input);

			//Assert
			result.Should().BeNull();
		}

		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParsePosition_WhenInvalidPositionEast_ThenReturnNull(string east)
		{
			//Arrange
			var input = new JsonIsoXmlPosition
			{
				PositionNorth = "54.588945",
				PositionEast = east,
				PositionStatus = "3"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParsePosition(input);

			//Assert
			result.Should().BeNull();
		}

		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParsePosition_InvalidPositionStatus_ThenReturnNull(string status)
		{
			//Arrange
			var input = new JsonIsoXmlPosition
			{
				PositionNorth = "54.588945",
				PositionEast = "9.989209",
				PositionStatus = status
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParsePosition(input);

			//Assert
			result.Should().BeNull();
		}

		[Test]
		public void ParsePosition_CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlPosition
			{
				PositionNorth = "54.588945",
				PositionEast = "9.989209",
				PositionStatus = "3"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParsePosition(input);

			//Assert
			result.Should().NotBeNull();
			result.PositionNorth.Should().Be(54.588945m);
			result.PositionEast.Should().Be(9.989209m);
			result.PositionStatus.Should().Be(3);
			result.PositionUp.Should().BeNull();
		}
	}
}