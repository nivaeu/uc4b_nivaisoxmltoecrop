﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	internal class ParseAttachedFileTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var sut = new JsonIsoXmlParser();
			var input = new JsonIsoXmlAttachedFile
			{
				FilenameWithExtension = "LINKLIST.XML",
				Preserve = "2",
				ManufacturerGln = "",
				FileType = "1"
			};

			//Act
			var result = sut.ParseAttachedFile(input);

			//Assert
			result.Should().NotBeNull();
			result.Preserve.Should().BeTrue();
			result.FileType.Should().Be(1);
			result.FilenameWithExtension.Should().Be("LINKLIST.XML");
			result.ManufacturerGln.Should().BeEmpty();
		}
	}
}