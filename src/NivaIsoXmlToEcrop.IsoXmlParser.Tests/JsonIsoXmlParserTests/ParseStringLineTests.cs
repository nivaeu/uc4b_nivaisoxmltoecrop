﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseLineStringTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlLineString
			{
				LineStringType = "1",
				Point = new JsonIsoXmlPoint[]
				{
					new JsonIsoXmlPoint {PointType = "2", PointNorth = "56.1966273645", PointEast = "10.1131725311"},
					new JsonIsoXmlPoint {PointType = "2", PointNorth = "56.2009247528", PointEast = "10.1143312454"},
					new JsonIsoXmlPoint {PointType = "2", PointNorth = "56.1966273645", PointEast = "10.1131725311"}
				}
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseLineString(input);

			//Assert
			result.Should().NotBeNull();
			result.Points.Length.Should().Be(3);
			result.LineStringType.Should().Be(1);
		}
	}
}