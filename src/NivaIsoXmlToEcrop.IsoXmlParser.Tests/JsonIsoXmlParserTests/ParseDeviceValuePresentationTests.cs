﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseDeviceValuePresentationTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlDeviceValuePresentation
			{
				DeviceValuePresentationObjectId = "99",
				Offset = "0",
				Scale = "0.0099999998",
				NumberOfDecimals = "0",
				UnitDesignator = "kg/ha"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDeviceValuePresentation(input);

			//Assert
			result.Should().NotBeNull();
			result.DeviceValuePresentationObjectId.Should().Be(99);
			result.Offset.Should().Be(0);
			result.Scale.Should().Be((decimal) 0.0099999998);
			result.NumberOfDecimals.Should().Be(0);
		}
	}
}