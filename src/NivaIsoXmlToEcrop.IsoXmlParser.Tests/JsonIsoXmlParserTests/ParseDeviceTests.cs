﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseDeviceTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlDevice
			{
				DeviceId = "DVC-27",
				DeviceDesignator = "Fertilizer Spreader",
				DeviceSoftwareVersion = "EDW2_ISO,1.21,2019-11-25",
				ClientName = "A00A80000B3594A2",
				DeviceStructureLabel = "00020101180011",
				DeviceLocalizationLabel = "FF000000006E65",
				DeviceElements = new JsonIsoXmlDeviceElement[]
				{
					new JsonIsoXmlDeviceElement()
				},
				DeviceProcessData = new JsonIsoXmlDeviceProcessData[]
				{
					new JsonIsoXmlDeviceProcessData()
				},
				DeviceValuePresentations = new JsonIsoXmlDeviceValuePresentation[]
				{
					new JsonIsoXmlDeviceValuePresentation()
				}
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDevice(input);

			//Assert
			result.Should().NotBeNull();
			result.DeviceId.Should().Be(27);
		}
	}
}