﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseTimeTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlTime
			{
				Start = "2021-04-07T16:22:09",
				Stop = "2021-04-07T16:50:22",
				Duration = "1693",
				Type = "4",
				Position = new[]
				{
					new JsonIsoXmlPosition
					{
						PositionNorth = "56.500093877",
						PositionEast = "10.4972617082",
						PositionUp = "88840",
						PositionStatus = "1"
					} 
				},
				DataLogValue = new[]
				{
					new JsonIsoXmlDataLogValue
					{
						ProcessDataDDI = "005B",
						ProcessDataValue = "0",
						DeviceElementIdRef = "DET-243"
					},
					new JsonIsoXmlDataLogValue
					{
						ProcessDataDDI = "0074",
						ProcessDataValue = "110132",
						DeviceElementIdRef = "DET-243"
					}
				}
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseTime(input);

			//Assert
			result.Should().NotBeNull();
			result.Duration.Should().Be(1693);
			result.Type.Should().Be(4);
			result.Position.Should().NotBeNull();
			result.DataLogValue.Should().NotBeNull();
			result.DataLogValue.Length.Should().Be(2);
		}

		[Test]
		public void CanParse_WithoutOptionalParameter()
		{
			//Arrange
			var input = new JsonIsoXmlTime
			{
				Start = "2021-04-07T16:22:09",
				Duration = "10",
				Type = "4"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseTime(input);

			//Assert
			result.Should().NotBeNull();
			result.Stop.Should().Be(DateTime.Parse("2021-04-07T16:22:19"));
			result.Position.Should().BeNull();
			result.DataLogValue.Should().BeNull();
		}

		[Test]
		public void CanParse_WithoutOptionalParameter2()
		{
			//Arrange
			var input = new JsonIsoXmlTime
			{
				Start = "2021-04-07T16:22:09",
				Stop = "2021-04-07T16:22:13",
				Type = "4"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseTime(input);

			//Assert
			result.Should().NotBeNull();
			result.Duration.Should().Be(4);
		}

		[Test]
		public void WrongType()
		{
			//Arrange
			var input = new JsonIsoXmlTime
			{
				Start = "2021-04-07T16:22:09",
				Stop = "2021-04-07T16:22:13",
				Type = "3"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseTime(input);

			//Assert
			result.Should().BeNull();
		}
	}
}