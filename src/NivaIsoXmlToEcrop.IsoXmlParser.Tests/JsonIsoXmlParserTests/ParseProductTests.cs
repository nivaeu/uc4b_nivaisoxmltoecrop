﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseProductTests
	{
		[Test]
		public void ParseProduct_WhenValidInput_ThenReturnIsoXmlProduct()
		{
			//Arrange
			var input = new JsonIsoXmlProduct()
			{
				ProductId = "PDT1",
				ProductDesignator = "AHL",
				QuantityDDI = "004B",
				ProductGroupIdRef = "PGP1"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseProduct(input);

			//Assert
			result.Should().NotBeNull();
			result.Id.Should().Be(1);
			result.ProductDesignator.Should().Be("AHL");
			result.DdiValue.Should().Be(75);
			result.ProductGroupIdRef.Should().Be(1);
		}

		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParseProduct_WhenInvalidProductDesignator_ThenReturnNull(string productDesignator)
		{
			//Arrange
			var input = new JsonIsoXmlProduct()
			{
				ProductId = "PDT1",
				ProductDesignator = productDesignator
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseProduct(input);

			//Assert
			result.Should().BeNull();
		}

		[TestCase("")]
		[TestCase(null)]
		[TestCase(" ")]
		public void ParseProduct_WhenInvalidId_ThenReturnNull(string id)
		{
			//Arrange
			var input = new JsonIsoXmlProduct()
			{
				ProductId = id,
				ProductDesignator = "AHL"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseProduct(input);

			//Assert
			result.Should().BeNull();
		}
	}
}