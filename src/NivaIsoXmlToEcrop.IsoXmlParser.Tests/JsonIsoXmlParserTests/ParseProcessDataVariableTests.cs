﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseProcessDataVariableTests
	{
		[Test]
		public void ParseProcessDataVariable_WhenValidInput_ThenReturnProcessDataVariable()
		{
			//Arrange
			var input = new JsonIsoXmlProcessDataVariable
			{
				ProcessDataDDI = "0001",
				ProcessDataValue = "920000",
				ProductIdRef = "PDT1",
				ValuePresentationIdRef = "VPN8"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseProcessDataVariable(input);

			//Assert
			result.Should().NotBeNull();
			result.ProductIdRef.Should().Be(1);
			result.ProcessDataDDI.Should().Be(1);
			result.ProcessDataValue.Should().Be(920000);
			result.ValuePresentationIdRef.Should().Be(8);
		}
	}
}