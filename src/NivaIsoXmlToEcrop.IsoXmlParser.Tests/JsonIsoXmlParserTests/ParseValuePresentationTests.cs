﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseValuePresentationTests
	{
		[Test]
		public void ParseValuePresentation_CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlValuePresentation
			{
				ValuePresentationId = "VPN2",
				Offset = "0",
				Scale = "0.01",
				NumberOfDecimals = "2",
				UnitDesignator = "kg/hectare"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseValuePresentation(input);

			//Assert
			result.Should().NotBeNull();
			result.ValuePresentationId.Should().Be(2);
			result.Offset.Should().Be(0);
			result.Scale.Should().Be(0.01m);
			result.NumberOfDecimals.Should().Be(2);
			result.UnitDesignator.Should().Be("kg/hectare");
		}
	}
}