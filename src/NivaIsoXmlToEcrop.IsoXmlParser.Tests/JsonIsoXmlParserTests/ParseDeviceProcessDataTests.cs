﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.JsonIsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests.JsonIsoXmlParserTests
{
	public class ParseDeviceProcessDataTests
	{
		[Test]
		public void CanParse()
		{
			//Arrange
			var input = new JsonIsoXmlDeviceProcessData
			{
				DeviceProcessDataObjectId = "30",
				DeviceProcessDataDDI = "0006",
				DeviceProcessDataProperty = "3",
				DeviceProcessDataTriggerMethods= "9",
				DeviceProcessDataDesignator = "Setpoint Rate",
				DeviceValuePresentationObjectId = "99"
			};
			var sut = new JsonIsoXmlParser();

			//Act
			var result = sut.ParseDeviceProcessData(input);
			
			//Assert
			result.Should().NotBeNull();
			result.DeviceProcessDataObjectId.Should().Be(30);
			result.DeviceProcessDataDDI.Should().Be(6);
			result.DeviceProcessDataProperty.Should().Be(3);
			result.DeviceProcessDataTriggerMethods.Should().Be(9);
			result.DeviceProcessDataDesignator.Should().Be("Setpoint Rate");
			result.DeviceValuePresentationObjectId.Should().Be(99);
		}
	}
}