﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests
{
	internal class LinkListParserTests
	{
		[Test]
		public void ParseLinkList()
		{
			//Arrange
			var extraFiles = new Dictionary<string, byte[]>
			{
				{"LINKLIST.XML", File.ReadAllBytes("TestData/LINKLIST.XML")}
			};
			var input = new IsoXmlFilesInput
			{
				TaskDataXml = new byte[0],
				GridFiles = new Dictionary<string, byte[]>(),
				TimeLogFiles = new Dictionary<string, (byte[] Xml, byte[] Bin)>(),
				ExtraFiles = extraFiles
			};
			var sut = new IsoXmlLinkListParser();

			//Act
			var result = sut.Parse(input);

			//Assert
			result.Should().NotBeNull();
			result.ManagementSoftwareManufacturer.Should().Be("SEGES.dk");
			result.LinkGroup.Should().NotBeNull();
			result.LinkGroup.Length.Should().Be(1);
			var linkGroup = result.LinkGroup.First();
			linkGroup.Id.Should().Be(1);
			linkGroup.Type.Should().Be(3);
			linkGroup.ManufacturerGln.Should().BeNullOrEmpty();
			linkGroup.LinkGroupNamespace.Should().Be("https://cropmanager.dk/");
			linkGroup.Links.Length.Should().Be(10);
			var link = linkGroup.Links.First();
			link.Id.Should().Be(1);
			link.Type.Should().Be("TSK");
			link.Value.Should().Be("4");
		}

		[Test]
		public void WhenNoLinkList_ThenReturnNull()
		{
			//Arrange
			var input = new IsoXmlFilesInput();
			var sut = new IsoXmlLinkListParser();

			//Act
			var result = sut.Parse(input);

			//Assert
			result.Should().BeNull();
		}
	}
}