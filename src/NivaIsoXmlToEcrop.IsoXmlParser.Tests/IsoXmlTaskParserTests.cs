﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests
{
	public class IsoXmlTaskParserTests
	{
		[Test]
		public void ParseTask()
		{
			//Arrange
			var bytes = File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TASKDATA.XML");
			var sut = new IsoXmlTaskParser();

			//Act
			var result = sut.ParseTask(bytes);

			//Assert
			result.Should().NotBeNull();
			result.TaskData.Should().NotBeNull();
			result.TaskData.VersionMajor.Should().Be("3");
			result.TaskData.VersionMinor.Should().Be("3");
			result.TaskData.TaskControllerManufacturer.Should().Be("AGCO GmbH / Fendt");
			result.TaskData.TaskControllerVersion.Should().Be("1.0");
			result.TaskData.DataTransferOrigin.Should().Be("2");

			result.TaskData.Tasks.Should().NotBeNull();
			result.TaskData.Tasks.Length.Should().Be(3);
			var task = result.TaskData.Tasks[0];
			task.TaskId.Should().Be("TSK2");
			task.TaskStatus.Should().Be("4");

			task.DataLogTrigger.Should().NotBeNull();
			task.DataLogTrigger[0].DataLogDDI.Should().Be("DFFF");
			task.DataLogTrigger[0].DataLogMethod.Should().Be("31");
			task.Time.Should().NotBeNull();
			task.Time.Length.Should().Be(2);
			var time = task.Time[1];
			time.Start.Should().Be("2021-04-07T16:22:09");
			time.Stop.Should().Be("2021-04-07T16:50:22");
			time.Type.Should().Be("4");
			time.DataLogValue.Length.Should().Be(16);
			time.DataLogValue[0].ProcessDataDDI.Should().Be("005B");
			time.DataLogValue[0].ProcessDataValue.Should().Be("0");
			time.DataLogValue[0].DeviceElementIdRef.Should().Be("DET-243");
			task.TimeLog.Should().NotBeNull();
			task.TimeLog.Length.Should().Be(1);
			task.TimeLog[0].Filename.Should().Be("TLG00001");
			task.TimeLog[0].TimeLogType.Should().Be("1");
			task.DeviceAllocation.Should().NotBeNull();
			task.DeviceAllocation.Length.Should().Be(4);
			task.DeviceAllocation[0].ClientNameValue.Should().Be("A00086000CC00001");
			task.DeviceAllocation[0].DeviceIdRef.Should().Be("DVC-26");
			task.DeviceAllocation[0].AllocationStamp.Should().NotBeNull();
		}

		[Test]
		public void ParseJsonIsoXml()
		{
			//Arrange
			var gridFiles = new Dictionary<string, byte[]>
			{
				{"GRD00000", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00000.bin")},
				{"GRD00001", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00001.bin")},
				{"GRD00002", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00002.bin")}
			};
			var timeLogs = new Dictionary<string, (byte[], byte[])>
			{
				{"TLG00001", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.bin"))
				},
				{"TLG00002", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.bin"))},
				{"TLG00003", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.bin"))},
				{"TLG00004", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.bin"))}
			};
			var input = new IsoXmlFilesInput
			{
				TaskDataXml = File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TASKDATA.XML"),
				GridFiles = gridFiles,
				TimeLogFiles = timeLogs
			};
			var sut = new IsoXmlTaskParser();

			//Act
			var result = sut.ParseJsonIsoXml(input);

			//Assert
			result.Should().NotBeNull();
			result.TaskData.Tasks.Sum(x => x.TimeLog.Length).Should().Be(4);
			result.TaskData.Tasks.Sum(x => x.Time.Length).Should().Be(4132);
			result.TaskData.Tasks[0].Time.Sum(x => x.DataLogValue?.Length ?? 0).Should().Be(26199);
			result.TaskData.Tasks[0].Time.Sum(x => x.Position?.Length ?? 0).Should().Be(1009);
			var dataLogValues = result.TaskData.Tasks[0].Time.Where(k => k.DataLogValue != null).SelectMany(d => d.DataLogValue).GroupBy(d => d.ProcessDataDDI);
			var ddiWithCount = dataLogValues.Select(s => (int.Parse(s.Key, NumberStyles.HexNumber), s.Count())).ToArray();
			ddiWithCount.Length.Should().Be(27);
			ddiWithCount[14].Item2.Should().Be(1007);
		}
	}
}