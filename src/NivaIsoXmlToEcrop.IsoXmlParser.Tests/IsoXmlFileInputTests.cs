﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests
{
	internal class IsoXmlFileInputTests
	{
		[Test]
		public void CanExtractZip()
		{
			//Arrange
			var path = "TestData/task1.zip";
			using var fileStream = new FileStream(path, FileMode.Open);

			//Act
			var result = IsoXmlFilesInput.Create(fileStream);

			//Assert
			result.Should().NotBeNull();
			result.TaskDataXml.Should().NotBeNull();
			result.TimeLogFiles.Count.Should().Be(4);
			result.ExtraFiles.Count.Should().Be(1);
			result.GridFiles.Count.Should().Be(3);
		}
	}
}