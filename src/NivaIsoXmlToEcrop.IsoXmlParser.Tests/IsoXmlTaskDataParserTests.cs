﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FluentAssertions;
using NivaIsoXmlToEcrop.IsoXmlParser.IsoXmlModels;
using NUnit.Framework;

namespace NivaIsoXmlToEcrop.IsoXmlParser.Tests
{
	public class IsoXmlTaskDataParserTests
	{
		[Test]
		public void CanParseIsoXml()
		{
			//Arrange
			var gridFiles = new Dictionary<string, byte[]>
			{
				{"GRD00000", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00000.bin")},
				{"GRD00001", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00001.bin")},
				{"GRD00002", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00002.bin")}
			};
			var timeLogs = new Dictionary<string, (byte[], byte[])>
			{
				{"TLG00001", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.xml"),
						File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.bin"))
				},
				{"TLG00002", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.bin"))},
				{"TLG00003", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.bin"))},
				{"TLG00004", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.bin"))}
			};
			var input = new IsoXmlFilesInput
			{
				TaskDataXml = File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TASKDATA.XML"),
				GridFiles = gridFiles,
				TimeLogFiles = timeLogs
			};

			//Act
			var result = new IsoXmlTaskDataParser().Parse(input);

			//Assert
			result.Should().NotBeNull();
			result.Tasks.Sum(x => x.Times.Length).Should().Be(4132);
			result.Tasks[0].Times.Sum(x => x.DataLogValue?.Length ?? 0).Should().Be(26199);
			result.Tasks[0].Times.Sum(x => x.Position?.Length ?? 0).Should().Be(1009);
			var dataLogValues = result.Tasks[0].Times
				.Where(k => k.DataLogValue != null)
				.SelectMany(d => d.DataLogValue)
				.Select(d => d.ProcessDataDdi)
				.Distinct()
				.ToList();
			dataLogValues.Count.Should().Be(27);
		}

		[Test]
		[Ignore("Currently make a csv file from isoXml data. Use to test parser in QGIS")]
		public void WriteParsedIsoXmlToCsvFile()
		{
			var gridFiles = new Dictionary<string, byte[]>
			{
				{"GRD00000", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00000.bin")},
				{"GRD00001", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00001.bin")},
				{"GRD00002", File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/GRD00002.bin")}
			};
			var timeLogs = new Dictionary<string, (byte[], byte[])>
			{
				{"TLG00001", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.xml"),
						File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00001.bin"))
				},
				{"TLG00002", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00002.bin"))},
				{"TLG00003", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00003.bin"))},
				{"TLG00004", (File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.xml"),
					File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TLG00004.bin"))}
			};
			var input = new IsoXmlFilesInput
			{
				TaskDataXml = File.ReadAllBytes("TestData/210407_180315_exported_TaskData0_3/TASKDATA.XML"),
				GridFiles = gridFiles,
				TimeLogFiles = timeLogs
			};

			var result = new IsoXmlTaskDataParser().Parse(input);

			PrintEffectiveTime(result);
		}

		private void PrintEffectiveTime(IsoXmlTaskData result)
		{
			foreach (var task in result.Tasks)
			{
				var dataLogValues = task.Times
					.Where(k => k?.DataLogValue != null && k.Type == RecordTime.Effective)
					.SelectMany(d => d.DataLogValue)
					.Select(f => f.ProcessDataDdi)
					.Distinct()
					.OrderBy(d => d)
					.ToArray();

				const string separator = ";";
				var dataLogHeaderData = string.Join(separator, dataLogValues);
				var stringBuilder = new StringBuilder();
				stringBuilder.AppendLine($"north{separator}east{separator}start{separator}{dataLogHeaderData}");
				foreach (var isoXmlTime in task.Times)
				{
					if (isoXmlTime.Type != RecordTime.Effective)
					{
						continue;
					}

					var isoXmlPosition = isoXmlTime.Position?[0];
					var dictionary = new Dictionary<int, long?>();
					foreach (var dataLogValue in dataLogValues)
					{
						dictionary.Add(dataLogValue, null);
					}

					foreach (var isoXmlDataLogValue in isoXmlTime.DataLogValue)
					{
						dictionary[isoXmlDataLogValue.ProcessDataDdi] = isoXmlDataLogValue.ProcessDataValue;
					}

					var temp4 = string.Join(separator, dictionary.Values);
					stringBuilder.AppendLine(
						$"{isoXmlPosition?.PositionNorth}{separator}{isoXmlPosition?.PositionEast}{separator}{isoXmlTime.Start}{separator}{temp4}");
				}

				var filename = $"TSK{task.TaskId}";
				File.WriteAllText($"{filename}.txt", stringBuilder.ToString());
			}
		}
	}
}