# Introduction
This subproject is part of the ["New IACS Vision in Action” NIVA](https://www.niva4cap.eu/) project that delivers 
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to 
support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under 
grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects 
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

# NivaIsoXmlToEcrop
NivaIsoXmlToEcrop is a library for mapping parts of IsoXml to a EcropReportMessage.
This repository contains a proof of concept developed at SEGES (https://seges.dk/)

## Purpose

## Library Limits
The current implementation of library only supports the extraction of field polygon and Actual Mass Per Area Application Rate (DDI 7) from isoxml and map to EcropMessage.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
Necessary software to use this repository

- **Visual Studio 2019:** https://visualstudio.microsoft.com/downloads/
- **.NET Core 3.1:** https://dotnet.microsoft.com/download/dotnet-core/3.1
- **git:** https://git-scm.com/downloads

## Installing

## Example

Mapper need a PartialEcropInput which contain 3 properties:
```csharp
public class EcropMapper
{
	public EcropMessage Map(PartialEcropInput input){}
}

public class PartialEcropInput
{
	public IsoXmlFilesInput IsoXmlFiles { get; set; }
	public EcropMessage EcropMessage { get; set; }
	public int? IsoXmlTaskId { get; set; }
}
```


Example where use EcropMapper
```csharp
var ecropJson = File.ReadAllText("TestData/partialEcropMessage.json");
var partialEcropMessage = JsonConvert.DeserializeObject<EcropMessage>(ecropJson);
var path = "TestData/task.zip";
using var fileStream = new FileStream(path, FileMode.Open);
var isoXmlInput = IsoXmlFilesInput.Create(fileStream);

var input = new PartialEcropInput
{
	EcropMessage = partialEcropMessage,
	IsoXmlFiles = isoXmlInput,
	IsoXmlTaskId = 2
};
var mapper = new EcropMapper();
var result = mapper.Map(input);
```

## Running the tests
1. cd `<root directory>`
2. dotnet test

## License
EU-PL Nest framework is <a rel="nofollow noreferrer noopener" href="https://github.com/nestjs/nest/blob/master/LICENSE">MIT licensed</a>. 

![European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009](NIVA.jpg)
